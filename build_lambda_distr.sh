#!/usr/bin/env bash

test -f lambda/lambda.zip && mv lambda/lambda.zip lambda/lambda.zip.old
docker run --rm -it -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:python3.6
