#!/usr/bin/env bash

set -e

# /var/task

yum install -y zip
pip3 install -r /src/requirements.txt

pip3 install /src -t .

GLOBIGNORE=.:..; zip -9 -r /out/lambda * ; unset GLOBIGNORE
