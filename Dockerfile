FROM python:3-alpine

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app

RUN apk -U add --no-cache --virtual .build-deps \
    gcc musl-dev libffi-dev openssl-dev libxml2-dev libxslt-dev \
    make \
    && apk -U add --no-cache openssl libffi libxml2 libxslt \
    && pip3 install -r /app/requirements.txt \
    && apk del .build-deps

COPY ./ /app
