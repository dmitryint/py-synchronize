# -*- coding: utf-8 -*-

__pkg_name__ = 'py-synchronize'

__version__ = "0.8.0"

from synchronize.syncer import Syncer