from synchronize.targets import S3Target, BaseTarget
from synchronize.objects import FileObject
from mock import patch
import os
import unittest


class TestBaseTarget(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.root_dir = '/test'
        cls.bucket="test-buck"
        cls.access_key_id="ABCD"
        cls.secret_access_key="ewqeqw*&*#"
        cls.kms_key_id="23-123-de-32"

    def setUp(self):
        self.t = S3Target(
            bucket=self.bucket,
            root_dir=self.root_dir,
            access_key_id=self.access_key_id,
            secret_access_key=self.secret_access_key,
        )

    def test_is_instance(self):
        self.assertIsInstance(self.t, BaseTarget)

    def test_set_root_dir(self):
        self.assertEqual(self.t.root_dir, self.root_dir)

    def test_set_connected(self):
        self.assertFalse(self.t.connected)

    def test_set_bucket(self):
        self.assertEqual(self.t._bucket, self.bucket)

    def test_set_access_key_id(self):
        self.assertEqual(self.t._access_key_id, self.access_key_id)

    def test_set_secret_access_key(self):
        self.assertEqual(self.t._secret_access_key, self.secret_access_key)

    def test_get_prefix(self):
        self.assertEqual(self.t._get_prefix_name('/test'), 'test/')

        with self.assertRaises(RuntimeError) as context:
            self.assertEqual(self.t._get_prefix_name('/'), '/')

    def test_write_file(self):
        pass