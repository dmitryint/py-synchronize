from synchronize.targets import BaseTarget
import unittest


class TestBaseTarget(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.root_dir = '/test'

    def setUp(self):
        self.t = BaseTarget(
            root_dir=self.root_dir,
        )

    def test_is_instance(self):
        self.assertIsInstance(self.t, BaseTarget)

    def test_set_root_dir(self):
        self.assertEqual(self.t.root_dir, self.root_dir)

    def test_set_connected(self):
        self.assertFalse(self.t.connected)

    def test_normpath(self):
        self.assertEqual(self.t._normpath(''), '/test')

        self.assertEqual(self.t._normpath('/test/1'), '/test/1')

        self.assertEqual(self.t._normpath('/test'), '/test')

        self.assertEqual(self.t._normpath('1'), '/test/1')

        with self.assertRaises(RuntimeError) as context:
            self.t._normpath('/test2')

        with self.assertRaises(RuntimeError) as context:
            self.t._normpath('../test2')

        with self.assertRaises(RuntimeError) as context:
            self.t._normpath('/')

        rt = BaseTarget(
            root_dir='/',
        )

        self.assertEqual(rt._normpath(''), '/')

        self.assertEqual(rt._normpath('/test/1'), '/test/1')

        self.assertEqual(rt._normpath('1'), '/1')

        self.assertEqual(rt._normpath('/'), '/')

    def test_set_options(self):
        t = BaseTarget(
            root_dir=self.root_dir,
            options={
                'test_1': False,
            }
        )

        self.assertFalse(t.get_option('test_1', default=True))
        self.assertTrue(t.get_option('test_non_existent', default=True))
