from synchronize.targets.crawlers import FreedomHealthTarget
import unittest
import os


class TestFreedomHealthTarget(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.root_dir = '/test'

    def setUp(self):
        self.t = FreedomHealthTarget(
            root_dir=self.root_dir,
            username = 'user',
            password = 'password'
        )

    def test_get_content_disposition_filename(self):
        h = 'attachment;filename="ALL MEASURES REQUIRED_10/19/2017.csv"'
        self.assertEqual(self.t._get_content_disposition_filename(h), 'ALL MEASURES REQUIRED_10-19-2017.csv')
