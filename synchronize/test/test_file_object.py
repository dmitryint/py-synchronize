from synchronize.objects import FileObject, GenericObject
import os
import unittest


class TestFileObject(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._key = 'test'
        cls._stat = os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            2345,  # size
            0,  # atime
            123456,  # mtime
            0,  # ctime
        ))

    def setUp(self):
        self.t = FileObject(
            key=self._key,
            stat=self._stat
        )

    def test_is_instance(self):
        self.assertIsInstance(self.t, GenericObject)
        self.assertIsInstance(self.t, FileObject)

    def test_is_file(self):
        self.assertTrue(self.t.is_file())

    def test_is_dir(self):
        self.assertFalse(self.t.is_dir())

    def test_get_key(self):
        self.assertEqual(self.t.key, self._key)

    def test_get_name(self):
        self.assertEqual(self.t.name, self._key)

    def test_get_dirname(self):
        self.assertEqual(self.t.dirname, '')

    def test_set_stat(self):
        self.assertEqual(self.t._stat, self._stat)

    def test_get_mtime(self):
        self.assertEqual(self.t.mtime, self._stat.st_mtime)
