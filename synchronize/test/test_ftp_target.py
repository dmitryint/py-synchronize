from synchronize.targets import FtpTarget, BaseTarget
from synchronize.objects import FileObject
from mock import patch, MagicMock, call
import ftplib
import unittest


class TestFtpTarget(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.root_dir = '/test'
        cls.hostname="localhost"

    def setUp(self):
        self.t = FtpTarget(
            host=self.hostname,
            root_dir=self.root_dir,
        )

    def tearDown(self):
        def __close():
            pass
        self.t.close = __close

    def test_is_instance(self):
        self.assertIsInstance(self.t, BaseTarget)

    def test_set_root_dir(self):
        self.assertEqual(self.t.root_dir, self.root_dir)

    def test_read_only(self):
        self.assertFalse(self.t.read_only)

    def test_set_connected(self):
        self.assertFalse(self.t.connected)

    def test_set_tls(self):
        def __close():
            pass

        t = FtpTarget(
            host=self.hostname,
            root_dir=self.root_dir,
        )
        t.close = __close
        self.assertFalse(t._tls)

        t = FtpTarget(
            host=self.hostname,
            root_dir=self.root_dir,
            tls=True,
        )
        t.close = __close
        self.assertTrue(t._tls)

        t = FtpTarget(
            host=self.hostname,
            root_dir=self.root_dir,
            implict_tls=True,
        )
        t.close = __close
        self.assertTrue(t._tls)

    @patch("synchronize.targets.FtpTarget.mkdir")
    def test_makedirs_1(self, mkdir_f):
        self.t._makedirs('/test')
        self.assertListEqual(mkdir_f.call_args_list, [call('/test')])

        mkdir_f.reset_mock()
        self.t._makedirs('/test/123')
        self.assertListEqual(mkdir_f.call_args_list, [call('/test'), call('/test/123')])

        mkdir_f.reset_mock()
        self.t._makedirs('/test/123/test2 dir')
        self.assertListEqual(mkdir_f.call_args_list, [call('/test'), call('/test/123'), call('/test/123/test2 dir')])
    #@patch('ftplib.FTP.retrlines')
    #def test_nlst(self, func):
    #    func.return_value = ''
    #    self.assertEqual(self.t._nlst('/'), [])
