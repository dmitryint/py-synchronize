from synchronize.targets.crawlers import CareplusHpTarget
import unittest
import os


class TestCareplusHpTarget(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.root_dir = '/test'

    def setUp(self):
        self.t = CareplusHpTarget(
            root_dir=self.root_dir,
            username = 'user',
            password = 'password'
        )

    def test_date_format(self):
        size = '444605'
        d = '8/14/2017 7:09:06 AM'  # 1502694546
        res = self.t._lstat(size, d)
        self.assertIsInstance(res, os.stat_result)

        self.assertEqual(res.st_ctime, 1502694546)
        self.assertEqual(res.st_mtime, 1502694546)
