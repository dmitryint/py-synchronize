# -*- coding: iso-8859-1 -*-
from __future__ import print_function
from synchronize.targets import BaseTarget
from synchronize.objects import FileObject
import logging
import sys
import time
__all__ = {'Syncer'}


class Syncer(object):
    def __init__(self, source: BaseTarget, destination: BaseTarget, pool=None, pool_func=None, options:dict=None) -> None:
        """
        :param source: instance of synchronize.targets.BaseTarget
        :param destination: instance of synchronize.targets.BaseTarget
        :param options: sync options
        """
        self.logger = options.get('logger', None)
        if not self.logger:
            # configure default logger to stdout
            log = logging.getLogger('dirsync')
            log.setLevel(logging.INFO)
            if not log.handlers:
                hdl = logging.StreamHandler(sys.stdout)
                hdl.setFormatter(logging.Formatter('%(message)s'))
                log.addHandler(hdl)
            self.logger = log

        self._pool = pool
        self._pool_func = pool_func

        self._source = source
        self._destination = destination

        self._copyfiles = True
        self._updatefiles = True
        self._creatdirs = True

        self._verbose = True

        self._changed = []
        self._added = []
        self._deleted = []

        try:
            self._source.open(main_thread=True)
        except Exception as e:
            logging.exception(
                "Exception: %s" % e,
                  extra={
                      "operation_type": "error",
                  }
            )
            raise

        try:
            self._destination.open(main_thread=True)
        except Exception as e:
            logging.exception(
                "Exception: %s" % e,
                  extra={
                      "operation_type": "error",
                  }
            )
            logging.shutdown()
            raise
        # TODO: try to open source
        # TODO: check that destination exists

    def __del__(self):
        self._source.close()
        self._destination.close()

    def log(self, msg=''):
        self.logger.info(msg)

    def do_work(self):
        pass

    def _compare(self, tgt1:BaseTarget, tgt2:BaseTarget):
        pass

    def _copy_file(self, src:BaseTarget, dest:BaseTarget, blob:FileObject):
        assert isinstance(blob, FileObject)

        if self._verbose:
            logging.info(
                'Copying file: %s from %s to %s' % (blob.key, src, dest),
                extra={
                    "operation_type": "copy",
                    "blob_name": "%s" % blob.key,
                    "copy_from": "%s" % src,
                    "copy_to": "%s" % dest,
                    "latest": "%s" % blob.is_latest,
                }
            )

        def __block_written(data):
            pass
        with src.open_readable(blob) as fp_src:
            dest.write_file(blob, fp_src, callback=__block_written)

    def _copy(self):
        for el in self._source.walk():
            if el.is_file():
                dst_el = self._destination.get_object(el.dst_key or el.key)
                if not dst_el:
                    self._added.append(el)
                    yield el
                elif el > dst_el:
                    self._changed.append(el)
                    yield el
                elif int(el.size) != int(dst_el.size):
                    if self._source.get_option('compare_size', default=True):
                        logging.warning('Warning: %s size missmatch: %s!=%s' %(el.key, el.size, dst_el.size))
                        self._changed.append(el)
                        yield el

    def copy(self):
        start = time.time()

        if self._pool and self._source.pool_safe and self._destination.pool_safe:
            self._pool.map(self._pool_func, self._copy())
        else:
            for el in self._copy():
                self._copy_file(self._source, self._destination, el)


        elap = time.time() - start
        logging.info(
            'Elapsed time: %s' % elap,
            extra={
                "operation_type": "stats",
                "key_name": "elapsed",
                "key_value": "%s" % elap,
            }
        )
        logging.info(
            'New files added: %s' % len(self._added),
            extra = {
                "operation_type": "stats",
                "key_name": "added",
                "key_value": "%s" % len(self._added),
            }
        )
        logging.info(
            'Updated files: %s' % len(self._changed),
            extra={
                "operation_type": "stats",
                "key_name": "updated",
                "key_value": "%s" % len(self._changed),
            }
        )
