# -*- coding: iso-8859-1 -*-
from synchronize.const import DEFAULT_BLOCKSIZE
from synchronize.objects import FileObject, DirectoryObject
from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
from datetime import datetime
import ftplib
import calendar
import logging
from tempfile import SpooledTemporaryFile
import os
from posixpath import join as posix_join, dirname as posix_dirname, basename as posix_basename, sep as posix_sep
import pylru
import re
import socket
import time
__all__ = {'FtpTarget'}


class FtpTarget(BaseTarget):
    def __init__(self, root_dir: str, host:str, port:int=0, username:str=None, password:str=None,
                 tls: bool=False, implict_tls: bool=False, passive: bool=False, timeout: int=None, options: dict=None, read_only: bool=False):
        super(FtpTarget, self).__init__(root_dir, options, read_only)
        self._get_conn = self._get_ftp_connection(
            root_dir=root_dir,
            host=host,
            port=port or 0,
            username=username,
            password=password,
            tls=tls,
            implict_tls=implict_tls,
            passive=passive,
            timeout=timeout
        )
        self._ftp = None
        self.ftp_socket_connected = False
        self._host = host
        self._tls = tls or implict_tls
        self._cache = pylru.lrucache(1000)
        self._neg_cache = pylru.lrucache(1000)

    def __str__(self) -> str:
        return "<FTP:%s>" % self.get_base_name()

    def __repr__(self) -> str:
        return "<FTP:%s>" % self.get_base_name()

    def _get_ftp_connection(self, root_dir: str, host:str, port:int=0, username:str=None, password:str=None,
                 tls:bool=False, implict_tls:bool=False, passive:bool=False, timeout:int=300):
        def inner_func():
            conn = None
            if tls:
                from ..patches import FTP_TLS_WIN
                try:
                    conn = self.get_option('patch_win_tls') and FTP_TLS_WIN() or ftplib.FTP_TLS()
                except AttributeError:
                    print("Python 2.7/3.2+ required for FTPS (TLS).")
                    raise
            elif implict_tls:
                from ..patches import IMPL_FTP_TLS, IMPL_FTP_TLS_WIN
                try:
                    conn = self.get_option('patch_win_tls') and IMPL_FTP_TLS_WIN() or IMPL_FTP_TLS()
                except AttributeError:
                    print("Python 2.7/3.2+ required for FTPS (TLS).")
                    raise
            else:
                conn = ftplib.FTP()

            if timeout:
                conn.connect(host, port, timeout)
            else:
                # Py2.7 uses -999 as default for `timeout`, Py3 uses None
                conn.connect(host, port)
            try:
                # Login (as 'anonymous' if self.username is undefined):
                conn.login(username, password)
            except ftplib.error_perm as e:
                print(e)
                raise

            if tls or implict_tls:
                # Upgrade data connection to TLS.
                conn.prot_p()

            # Switch to passive mode
            conn.set_pasv(passive)
            return conn

        return inner_func

    def get_base_name(self) -> str:
        scheme = "ftps" if self._tls else "ftp"
        return "%s://%s%s" % (scheme, self._host, self.root_dir)

    def open(self, main_thread=False):
        self._ftp = self._get_conn()
        self.ftp_socket_connected = True
        self.connected = True
        return self

    def close(self):
        if self.ftp_socket_connected:
            try:
                self._ftp.quit()
            except ftplib.error_temp as e:
                if e.args[0].startswith("421"):
                    logging.warning("Connection was closed due time out. But we did all the work.")
                else:
                    raise
            except socket.error as e:
                print("Error: " + str(e))
            self.ftp_socket_connected = False

        self.connected = False

    def get_id(self):
        return self._host + self.root_dir

    def mkdir(self, dir_name: str) -> None:
        assert not self.read_only
        self._ftp.mkd(dir_name)
        self._neg_cache.clear()

    def _parse_list(self, lines:list) -> list:
        parsed_res = []
        for line in lines:
            m = re.match(
                r"^(?P<mode>[drwxsl-]+)\s+(?P<link_num>\d+)\s+(?P<uid>\w+)\s+(?P<gid>\w+)\s+(?P<size>\d+)\s+(?P<month>\w+)\s+(?P<day>\d{1,2})\s+((?P<hours>\d{1,2}):(?P<minutes>\d{1,2})|(?P<year>\d{4}))\s(?P<file_name>.*)$",
                line)
            if m:
                d = m.groupdict()
                if d['file_name'] != '.' and d['file_name'] != '..':
                    if d['year'] is None:
                        d['year'] = "%s" % datetime.utcnow().year
                    if d['hours'] is None:
                        d['hours'] = "00"
                        d['minutes'] = "00"
                    d['time_stamp'] = calendar.timegm(time.strptime(
                        "{day} {month} {year} {hours}:{minutes}:00".format(**d),
                        "%d %b %Y %H:%M:%S"
                    ))
                    parsed_res.append(d)
        return parsed_res

    def _nlst(self, args) -> list:
        '''Return a list of files in a given directory (default the current).'''
        return [el['file_name'] for el in self._list(args)]

    def _is_el_in_neg_cache(self, path):
        return any([True for el in self._neg_cache.keys() if el == path or path.startswith("%s/" % el)])

    def _list(self, args):
        '''Return a list of files in a given directory (default the current).'''
        files = []
        if self._is_el_in_neg_cache(args):
            # TODO:  raise file not found exception
            pass
        elif args in self._cache:
            files = self._cache[args]
        else:
            cmd = 'LIST %s' % args
            proceed = True
            while proceed:
                proceed = False
                try:
                    self._ftp.retrlines(cmd, files.append)
                except ftplib.error_temp as e:
                    if e.args[0].startswith("421"):
                        logging.warning("Connection was closed due time out.\nTrying to reopen...")
                        self.open()
                        files = []
                        proceed = True
                    else:
                        raise
                except socket.error as e:
                    logging.warning("Connection was closed due time out.\nTrying to reopen...")
                    self.open()
                    files = []
                    proceed = True
                except ftplib.error_perm as e:
                    if e.args[0].startswith("550"):
                        logging.debug('NEG_CACHE: %s' % args)
                        files = []
                        self._neg_cache[args] = True
                    else:
                        raise
                else:
                    self._cache[args] = files
        return self._parse_list(files)

    @do_not_run_if_dry
    def rmdir(self, dir_name: str) -> None:
        """Recursively delete a directory tree on a remote server."""
        assert not self.read_only
        wd = self._ftp.pwd()

        try:
            names = self._nlst(dir_name)
        except ftplib.all_errors as e:
            # some FTP servers complain when you try and list non-existent paths
            logging.error('rmdir: Could not remove {0}: {1}'.format(dir_name, e))
            return

        for name in names:
            if os.path.split(name)[1] in ('.', '..'): continue

            logging.info('rmdir: Checking {0}'.format(name))

            try:
                self._ftp.cwd(name)  # if we can cwd to it, it's a folder
                self._ftp.cwd(wd)  # don't try a nuke a folder we're in
                self.rmdir(name)
            except ftplib.all_errors:
                self._ftp.delete(name)
        try:
            self._ftp.rmd(dir_name)
        except ftplib.all_errors as e:
            logging.error('rmdir: Could not remove {0}: {1}'.format(dir_name, e))
        self._cache.clear()

    def _lstat(self, dir_name: str) -> os.stat_result:
        mtime = ctime = self._mtime(dir_name)
        size = self._size(dir_name) or 0
        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            size,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _isdir(self, path: str) -> bool:
        f = posix_basename(path)
        dir = posix_dirname(path)
        res = False
        for el in self._list(dir):
            if el['file_name'] == f:
                res = 'd' in el['mode']
                break
        return res

    def _isfile(self, path: str) -> bool:
        f = posix_basename(path)
        dir = posix_dirname(path)
        res = False
        for el in self._list(dir):
            if el['file_name'] == f:
                res = 'd' not in el['mode']
                break
        return res

    def _size(self, path: str):
        dir = posix_dirname(path)
        f = posix_basename(path)
        # TODO: handle symlinks
        size = None
        for el in self._list(dir):
            if el['file_name'] == f:
                size = el['size']
                break
        return size

    def _mtime(self, path: str):
        dir = posix_dirname(path)
        f = posix_basename(path)
        # TODO: handle symlinks
        mtime = None
        for el in self._list(dir):
            if el['file_name'] == f:
                mtime = el['time_stamp']
        return mtime

    def get_dir(self, dir_name: str) -> list:
        norm_dir_name = self._normpath(dir_name)
        for name in self._nlst(norm_dir_name):
            path = posix_join(norm_dir_name, name)
            # Get the relative path from the self.root_dir
            key = os.path.relpath(path, self.root_dir)
            yield self.get_object(key)
        return

    def get_object(self, key: str):
        path = self._normpath(key)
        res = None
        if self._isfile(path):
            res = FileObject(
                os.path.relpath(path, self.root_dir),
                self._lstat(path)
            )
        elif self._isdir(path):
            res = DirectoryObject(
                os.path.relpath(path, self.root_dir),
                self._lstat(path)
            )
        return res

    def open_readable(self, file_object:FileObject):
        assert isinstance(file_object, FileObject)
        """Open cur_dir/name for reading."""
        out = SpooledTemporaryFile(4 * 1024000)

        try:
            self._ftp.retrbinary("RETR %s" % self._normpath(file_object.dst_key or file_object.key), out.write)
        except ftplib.error_temp as e:
            if e.args[0].startswith("421"):
                logging.warning("Connection was closed due time out.\nTrying to reopen...")
                self.open()

                out = SpooledTemporaryFile(4 * 1024000)
                self._ftp.retrbinary("RETR %s" % self._normpath(file_object.dst_key or file_object.key), out.write)
            else:
                raise

        out.flush()
        out.seek(0)
        return out

    def _makedirs(self, dir_name: str):
        assert not self.read_only
        if dir_name in self._cache:
            return

        try:
            self._ftp.cwd(dir_name)
        except:
            logging.debug("Dir doen't exists: %s" % dir_name)
        else:
            logging.debug("Dir exists: %s" % dir_name)
            return
        parts = dir_name.split(posix_sep)
        cdir = ""
        for dir in parts:
            cdir += dir + "/"
            # No point in trying to create the directory again when we only
            # added a slash.
            if not dir:
                continue
            try:
                # remove trailing slash if len > 1
                self.mkdir(cdir.rstrip('/') if len(cdir) > 1 else cdir)
            except ftplib.Error as e:
                pass

    @do_not_run_if_dry
    def write_file(self, file_object: FileObject, fp_src, blocksize=DEFAULT_BLOCKSIZE, callback=None):
        assert not self.read_only
        assert isinstance(file_object, FileObject)
        dir_name = self._normpath(posix_dirname(file_object.dst_key or file_object.key)).rstrip('/')
        self._makedirs(dir_name)
        self._ftp.storbinary("STOR %s" % self._normpath(file_object.dst_key or file_object.key), fp_src, blocksize, callback)
        if dir_name in self._cache:
            del self._cache[dir_name]

    @do_not_run_if_dry
    def remove_file(self, file_object: FileObject):
        """Remove cur_dir/name."""
        assert not self.read_only
        assert isinstance(file_object, FileObject)
        dir_name = self._normpath(posix_dirname(file_object.dst_key or file_object.key)).rstrip('/')
        self._ftp.delete(self._normpath(file_object.key))
        if dir_name in self._cache:
            del self._cache[dir_name]
