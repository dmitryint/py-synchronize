# -*- coding: iso-8859-1 -*-
from __future__ import print_function
from synchronize.objects import FileObject, DirectoryObject, GenericObject
from synchronize.targets.decorators import do_not_run_if_dry
from posixpath import normpath, join as posix_join
import itertools
import logging
__all__ = {'BaseTarget'}


class BaseTarget(object):
    def __init__(self, root_dir: str, options:dict=None, read_only:bool=False) -> None:
        self.root_dir = root_dir
        self._read_only = False
        self._options=options
        self.connected = False
        logging.debug("BaseTarget: root_dir: %s read_only: %s options: %s connected: %s" %
        (
            self.root_dir,
            self._read_only,
            self._options,
            self.connected,
        ))

    def __del__(self):
        # TODO: http://pydev.blogspot.de/2015/01/creating-safe-cyclic-reference.html
        self.close()

    @property
    def dry_run(self):
        return self.get_option('dry_run', False)

    @property
    def pool_safe(self):
        return self.get_option('pool_safe', True)

    @property
    def read_only(self):
        return self._read_only

    def get_option(self, opt:str, default=None):
        return self._options.get(opt, default) if self._options else default

    def get_base_name(self):
        return "%s" % self.root_dir

    def open(self, main_thread=False):
        self.connected = True
        return self

    def close(self):
        self.connected = False

    def get_id(self):
        return self.root_dir

    def get_object(self, key: str):
        raise NotImplementedError

    def _normpath(self, dir_name: str) -> str:
        path = normpath(posix_join(self.root_dir, dir_name))
        if self.root_dir == path:
            pass
        elif dir_name and not self.root_dir.endswith('/') and not path.startswith("%s/" % self.root_dir):
            raise RuntimeError("Tried to navigate outside root %r: %r" % (self.root_dir, path))
        elif not path.startswith(self.root_dir):
            raise RuntimeError("Tried to navigate outside root %r: %r" % (self.root_dir, path))
        return path

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        raise NotImplementedError

    def rmdir(self, dir_name: str) -> None:
        """Remove cur_dir/name."""
        raise NotImplementedError

    def get_dir(self, dir_name: str) -> list:
        raise NotImplementedError

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        next_elements = self.get_dir('')
        flag = True
        while flag:
            counter = 0
            current, next_elements = itertools.tee(next_elements)
            intermediate = []
            for el in current:
                logging.debug("Element: %s" % el.key)
                yield el
                if isinstance(el, DirectoryObject):
                    counter+=1
                    intermediate.append(self.get_dir(el.key))
            next_elements = itertools.chain.from_iterable(intermediate)
            logging.debug("Iterations: %s" % counter)
            if counter == 0:
                flag = False

    def open_readable(self, file_object:FileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        raise NotImplementedError

    #def read_text(self, file_object:FileObject):
    #    """Read text string from cur_dir/name using open_readable()."""
    #    raise NotImplementedError

    @do_not_run_if_dry
    def write_file(self, file_object, fp_src, blocksize=8192, callback=None):
        """Write binary data from file-like to cur_dir/name."""
        raise NotImplementedError

    #def write_text(self, file_object:FileObject, s:str):
    #    """Write string data to cur_dir/name using write_file()."""
    #    raise NotImplementedError

    @do_not_run_if_dry
    def remove_file(self, file_object:FileObject) -> None:
        """Remove cur_dir/name."""
        raise NotImplementedError
