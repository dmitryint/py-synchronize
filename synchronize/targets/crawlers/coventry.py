from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import CrawlFileObject
from synchronize.utils import query_string_params_to_dict
from posixpath import join as posix_join
import calendar
import lxml.etree
import lxml.html
import os
import time
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs
__all__ = {'CoventryTarget'}


class CoventryTarget(BaseCrawlerTarget):
    """
    Coventry parser
    """
    def __init__(self, root_dir: str, username: str, password: str, options: dict=None) -> None:
        super(CoventryTarget, self).__init__(root_dir, options)
        self.root_dir = '/'
        parts = urlparse(
            'https://www.directprovider.com/providerPortalWeb/appmanager/coventry/extUsers')
        self._start_url = urlunparse((
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            "_nfpb=true&_windowLabel=portletInstance_6&portletInstance_6_actionOverride=/com/cvty/provider/portlet/login/login",
            parts.fragment
        ))
        self._username = username
        self._password = password
        self._session.headers.update(
            {
                'Host': 'www.directprovider.com',
                'Origin': 'https://www.directprovider.com',
            }
        )

    def open(self, main_thread=False):
        response = self._session.post(
            self._start_url,
            data={
                'portletInstance_6{actionForm.userId}': self._username,
                'portletInstance_6{actionForm.password}': self._password,
            }
        )
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        else:
            qs_dict = query_string_params_to_dict(urlparse(response.url).query)
            if '_pageLabel' in qs_dict and qs_dict['_pageLabel'] == "welcome_page_1":
                self.connected = True
            else:
                raise RuntimeError('Login failed')
        return self

    def _lstat(self, date: str) -> os.stat_result:
        """
        date:str 11/18/2014
        """
        mtime = ctime = calendar.timegm(time.strptime(
            "%s 00:00:00" % date, "%m/%d/%Y %H:%M:%S"))

        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            0,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _get_instance_34(self):
        page_label = dir_name = 'MSO_report'

        tax_id_number = "455087560"
        provider_data_string = "MEDPLAN CLINIC LLC - 1477956 - 455087560 - 08/01/2014 - No Term"
        provider_string = "MEDPLAN CLINIC LLC - 1477956 - 455087560 - 08/01/2014 - No Term"
        provider_id = "1477956|1477956"
        flag = "PR"

        # MSO/Med Practice Reports

        start_url_post = url = 'https://www.directprovider.com/providerPortalWeb/appmanager/coventry/extUsers'
        parts = urlparse(
            url
        )
        start_url_get = urlunparse((
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            "_nfpb=true&_pageLabel=MSO_report",
            parts.fragment
        ))

        response = self._session.get(start_url_get)
        """
        response = self._session.post(
            start_url_post,
            params={
                '_nfpb': 'true',
                '_windowLabel': 'portletInstance_34',
                'portletInstance_34_actionOverride': '/com/cvty/provider/portlet/planProvider/planProvSel',
            },
            data={
                'portletInstance_34wlw-select_key:{actionForm.tin}OldValue': 'true',
                'portletInstance_34wlw-select_key:{actionForm.tin}': tax_id_number,
                'systemId': '116',
                'dataString': provider_data_string,
                'portletInstance_34{actionForm.providerString}': provider_string,
                'portletInstance_34{actionForm.providerId}': provider_id,
                'portletInstance_34{actionForm.flag}': flag,
                'portletInstance_34{actionForm.providerTextFlag}': '',
            },
            headers={
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Referer': 'https://www.directprovider.com/providerPortalWeb/appmanager/coventry/extUsers?_nfpb=true&_windowLabel=portletInstance_34&portletInstance_34_actionOverride=%2Fcom%2Fcvty%2Fprovider%2Fportlet%2FplanProvider%2FplanProvSel',
            }
        )
        """

        qs_dict = query_string_params_to_dict(urlparse(response.url).query)
        if '_pageLabel' in qs_dict and qs_dict['_pageLabel'] == page_label:
            html = lxml.html.fromstring(response.content)
            conter = 0
            for file_name, file_link, file_date in zip(
                    html.xpath(
                        "//*[@id='results']/tbody/tr[contains(@class, 'row')]/td[1]/a/text()"),
                    html.xpath(
                        "//*[@id='results']/tbody/tr[contains(@class, 'row')]/td[1]/a/@href"),
                    html.xpath(
                        "//*[@id='results']/tbody/tr[contains(@class, 'row')]/td[2]/text()"),
            ):
                conter += 1
                file_name = file_name.strip()
                file_link = file_link.strip().replace('\n', '')
                file_date = file_date.strip()
                yield CrawlFileObject(
                    key=posix_join(dir_name, file_name),
                    stat=self._lstat(file_date),
                    link=file_link,
                )
            if conter==0:
                raise RuntimeError('Requested page have no records')
        else:
            raise RuntimeError('Error reaching the page: %s' % page_label)

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self._get_instance_34()

    def open_readable(self, file_object: CrawlFileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, CrawlFileObject)
        r = self._session.get(file_object.link, stream=True)
        if r.status_code == 200 and r.headers.get('Content-Disposition'):
            r.raw.decode_content = True
            return r.raw
        else:
            raise RuntimeError("Can't download file: %s, url: %s, error: %s, content: %s" % (
                file_object.key,
                file_object.link, 
                r.status_code,
                r.content))
