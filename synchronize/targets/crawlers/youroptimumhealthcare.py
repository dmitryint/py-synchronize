from synchronize.targets.crawlers.freedomhealth import FreedomHealthTarget
__all__ = {'YourOptimumHealthcareTarget'}


class YourOptimumHealthcareTarget(FreedomHealthTarget):
    HOST = 'apps.youroptimumhealthcare.com'
