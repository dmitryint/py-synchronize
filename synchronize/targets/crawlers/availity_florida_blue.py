# -*- coding: utf-8 -*-
from synchronize.targets.crawlers.availity_base import AvailityBaseTarget
from synchronize.objects import CrawlFileObject
from selenium.common.exceptions import TimeoutException
from posixpath import join as posix_join
import logging
import os
import time
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs


class AvailityFloridaBlueTarget(AvailityBaseTarget):

    def _get_population_based_analytics(self):
        dir_name = 'Quality and Efficiency Reporting/Population Based Analytics'
        intermediate_tab = None
        reports_tab = None

        def _download_detailed_report(td):
            td.click()
            self._click_by_xpath('//*[@id="ctl00_DefaultContent_RadMenu1_detached"]/ul/li//span[text() = "Member Per Line"]/../..', delay=1)

            # Inside detailed report
            self._driver.switch_to.default_content()
            self._switch_to_iframe()
            # Export to CSV
            self._click_by_xpath('//*[@id="DefaultContent_imgBtnCSV"]')
            self._wait_for_progress_bar()

        def _get_file_from_downloads_folder():
            downloading = True
            f_list = []
            time_counter = 0
            # TODO: improve
            while downloading:
                f_list = os.listdir(self._chrome_download_dir)
                if any(f_list):
                    downloading = not all([os.path.splitext(f)[1].lower() != '.crdownload' for f in f_list ])
                if downloading:
                    print('.')
                    time.sleep(1)
                    time_counter +=1
                if time_counter > self.MAX_WAIT_TIME:
                    raise TimeoutException

            f_list = os.listdir(self._chrome_download_dir)
            time_sorted_list = sorted(f_list, key=lambda x: os.path.getmtime(os.path.join(self._chrome_download_dir,x)))
            file_name = time_sorted_list[len(time_sorted_list) - 1]
            logging.warning('Found last downloaded file: %s' % os.path.join(self._chrome_download_dir,file_name))
            return file_name

        try:
            self._driver.switch_to.window(self._availity_home_tab)
            self._driver.get(self._availity_start_url)
            # Check that user is logged-in
            self._wait_for_class('user-account')

            # Payer Spaces / Florida Blue
            self._click_by_xpath('//*[@id="availity-secondary-navbar-collapse"]/ul[1]/li[5]/a')
            self._click_by_xpath('//*[@id="nav-item-73162546201440710195134200002269"]/div/a')

            # Open Resources tab
            self._switch_to_iframe()
            self._click_by_xpath('//*[@id="resources-tab"]', delay=1)

            # Click Florida Blue Passport Portal
            cur_tabs_count = len(self._driver.window_handles)
            self._click_by_xpath('//*[@id="linkout-73162546201440719962781200005787"]')

            intermediate_tab = self._wait_for_next_tab(cur_tabs_count)

            self._set_select_option_by_name(
                select_name='organizationDropdownContainer:organization',
                # TODO: change me
                option='Professional Health Choice'
            )

            cur_tabs_count = len(self._driver.window_handles)
            self._driver.find_element_by_id("submitButton").click()
            reports_tab = self._wait_for_next_tab(cur_tabs_count)

            # Accept Terms & Agreement
            self._click_by_xpath('//*[@id="disclaimerContainer"]')

            # Quality & Efficiency Reporting
            self._click_by_xpath('//*[@id="qerpHome"]')

            # Population Based Analytics
            self._click_by_xpath('//*[@id="qerpAnalysisTool"]')

            # Medicare Provider Group List
            self._switch_to_iframe()

            table = self._driver.find_element_by_xpath('//*[@id="ctl00_DefaultContent_rdgMedicareProviderGroup_ctl00"]')
            cur_pos = 0
            total = len(table.find_elements_by_xpath('.//tr[contains(@class, "rgRow") or contains(@class, "rgAltRow")]/td[11]'))

            # TODO: Iterate over pages.
            # Iterate over rows.
            for row in range(total):
                if row == cur_pos:
                    table = self._driver.find_element_by_xpath(
                        '//*[@id="ctl00_DefaultContent_rdgMedicareProviderGroup_ctl00"]')
                    td = table.find_elements_by_xpath(
                        './/tr[contains(@class, "rgRow") or contains(@class, "rgAltRow")]/td[11]')[cur_pos]
                    _download_detailed_report(td)
                    file_name = _get_file_from_downloads_folder()
                    yield CrawlFileObject(
                        key=posix_join(dir_name, file_name),
                        stat=os.lstat(os.path.join(self._chrome_download_dir,file_name)),
                        link=urljoin('file://', os.path.join(self._chrome_download_dir,file_name)),
                    )

                    cur_pos += 1

                    # return back
                    self._driver.switch_to.default_content()
                    self._click_by_xpath('//*[@id="ppBodyDiv"]/div[4]/div[6]/div/ul/li[4]/a')
                    self._switch_to_iframe()

        except Exception:
            self.close()
            raise
        else:
            if intermediate_tab:
                #close
                pass

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self._get_population_based_analytics()
