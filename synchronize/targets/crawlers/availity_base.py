# -*- coding: utf-8 -*-
from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import CrawlFileObject
from synchronize.utils import mkdir_p
from synchronize.const import MODULE_PATH
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.common.exceptions import TimeoutException, ElementNotVisibleException
import logging
import os
import platform
import requests
import shutil
from selenium import webdriver
import time
import uuid
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs


class AvailityBaseTarget(BaseCrawlerTarget):
    MAX_WAIT_TIME = 60

    def __init__(self, root_dir: str, username:str, password:str, options:dict=None) -> None:
        super(AvailityBaseTarget, self).__init__(root_dir, options)
        self.root_dir = '/'
        self._start_url = 'https://apps.availity.com/availity/web/public.elegant.login'
        self._username = username
        self._password = password

        self._tmp_dir_prefix = '/tmp'
        self._chrome_download_dir = os.path.join(self._tmp_dir_prefix, str(uuid.uuid4()))
        mkdir_p(self._chrome_download_dir)

        self._headless = False
        self._driver = None

        self._availity_home_tab = None
        self._availity_start_url = urljoin(self._start_url, '/public/apps/home/#!/')
        self._download_tab = None

    def __del__(self):
        self.close()
        shutil.rmtree(self._chrome_download_dir)

    def _get_webdriver_remote(self):
        from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
        prefs = {
            "profile.default_content_settings.popups": 0,
            "download.default_directory": self._chrome_download_dir,
            "directory_upgrade": True,
            "intl.accept_languages": "en,en_US",
        }

        driver = webdriver.Remote(
            command_executor="http://127.0.0.1:4444/wd/hub",
            desired_capabilities=DesiredCapabilities.CHROME)
        return driver

    def _get_webdriver(self):
        from selenium.webdriver.chrome.options import Options
        from splinter.driver.webdriver import BaseWebDriver
        DIR = os.path.join(MODULE_PATH, 'chromedriver')
        options = Options()

        prefs = {
            "profile.default_content_settings.popups": 0,
            "download.default_directory": self._chrome_download_dir,
            "directory_upgrade": True,
            "intl.accept_languages": "en,en_US",
        }

        options.add_experimental_option('prefs', prefs)
        if self._headless:
            options.add_argument('--headless')

        browser = BaseWebDriver()
        chromedriver_path = os.path.join(DIR, platform.system(), 'chromedriver')
        logging.warning('chromedriver: %s' % chromedriver_path)
        browser.driver = webdriver.Chrome(chromedriver_path, chrome_options=options)
        return browser.driver

    def open(self, main_thread=False):
        self._driver = self._get_webdriver()
        try:
            # Get login page
            self._driver.get(self._start_url)
            assert "Log In to Availity" in self._driver.title

            # Fill username/password
            username_inp = self._driver.find_element_by_id("userId")
            password_inp = self._driver.find_element_by_id("password")
            username_inp.send_keys(self._username)
            password_inp.send_keys(self._password)
            self._driver.find_element_by_id("loginFormSubmit").click()

            # Check that user is logged-in
            self._wait_for_class('user-account')

            assert self._driver.current_url == self._availity_start_url
            self.connected = True

            self._availity_home_tab = self._driver.current_window_handle
            #self._download_tab
            #self._driver.get('chrome://downloads/')

            # back to Availity tab
            self._driver.switch_to.window(self._availity_home_tab)
        except Exception:
            self._driver.quit()
            raise

    def close(self):
        if self.connected:
            self._driver.quit()
        self.connected = False

    def _click_by_xpath(self, xp, delay=None):
        try:
            try:
                element = WebDriverWait(self._driver, self.MAX_WAIT_TIME).until(
                    EC.presence_of_element_located((By.XPATH, xp)))
                logging.warning("Page is ready: %s Title: %s" % (self._driver.current_url, self._driver.title))
            except TimeoutException:
                logging.error("Loading took too much time! Timeout: %s" % self.MAX_WAIT_TIME)
                raise
            except Exception as e:
                logging.error("Exception: %s" % e)
                raise
            else:
                if delay:
                    time.sleep(delay)
                try:
                    element.click()
                except ElementNotVisibleException as e:
                    logging.error("%s:%s" %(e,xp))
        except Exception:
            self.close()
            raise

    def _switch_to_iframe(self):
        try:
            try:
                iframe = WebDriverWait(self._driver, self.MAX_WAIT_TIME).until(EC.presence_of_element_located((By.TAG_NAME, 'iframe')))
                logging.warning("Page is ready: %s" % self._driver.current_url)
            except TimeoutException:
                logging.error("Loading took too much time! Timeout: %s" % self.MAX_WAIT_TIME)
                raise
            except Exception as e:
                logging.error("Exception: %s" % e)
                raise
            else:
                self._driver.switch_to.frame(iframe)
        except Exception:
            self.close()
            raise

    def _wait_for_class(self, class_name):
        try:
            try:
                WebDriverWait(self._driver, self.MAX_WAIT_TIME).until(EC.presence_of_element_located((By.CLASS_NAME, class_name)))
                logging.warning("Page is ready: %s Title: %s" % (self._driver.current_url, self._driver.title))
            except TimeoutException:
                logging.error("Loading took too much time! Timeout: %s" % self.MAX_WAIT_TIME)
                raise
            except Exception as e:
                logging.error("Exception: %s" % e)
                raise
        except Exception:
            self.close()
            raise

    def _wait_for_progress_bar(self):
        try:
            try:
                WebDriverWait(self._driver, self.MAX_WAIT_TIME).until(
                    EC.invisibility_of_element_located((By.XPATH, '//*[@id="MasterLoadingPanel"]')))
            except TimeoutException:
                logging.error("Loading took too much time! Timeout: %s" % self.MAX_WAIT_TIME)
                raise
            except Exception as e:
                logging.error("Exception: %s" % e)
                raise
        except Exception:
            self.close()
            raise

    def _wait_for_next_tab(self, cur_tabs_count=None):
        if cur_tabs_count is None:
            cur_tabs_count = len(self._driver.window_handles)
        WebDriverWait(self._driver, self.MAX_WAIT_TIME).until(lambda d: len(d.window_handles) > cur_tabs_count)
        # switch to the last window handle
        # https://stackoverflow.com/questions/31457355/how-can-i-move-webdriver-to-new-opened-chrome-tab-without-knowing-index
        self._driver.switch_to.window(self._driver.window_handles[-1])
        return self._driver.current_window_handle

    def _set_select_option_by_name(self, select_name, option):
        select = Select(self._driver.find_element_by_name(select_name))
        select.select_by_visible_text(option)

    def _get_request_session_from_selenium(self)-> requests.Session:
        cookies = self._driver.get_cookies()
        session = requests.session()
        agent = self._driver.execute_script("return navigator.userAgent")
        session.headers.update(
            {
                'User-Agent': agent
            }
        )
        for cookie in cookies:
            session.cookies.set(cookie['name'], cookie['value'])
        return session

    def open_readable(self, file_object:CrawlFileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, CrawlFileObject)
        file_name = urlparse(file_object.link, allow_fragments=True).path
        fp = open(self._normpath(file_name), "rb")
        return fp
