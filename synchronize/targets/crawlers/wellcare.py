from synchronize.targets.crawlers.accureports import AccuReportsGenericTarget
__all__ = {'WellCareTarget'}


class WellCareTarget(AccuReportsGenericTarget):
    ACCU_HOST = 'wellcare.accureports.com'
