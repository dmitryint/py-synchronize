from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import CrawlFileObject
import calendar
import lxml.etree
import lxml.html
import os
import time
try:
    from urlparse import urlparse, urljoin
except:
    from urllib.parse import urlparse, urljoin
__all__ = {'CareplusHpTarget'}


class CareplusHpTarget(BaseCrawlerTarget):
    def __init__(self, root_dir: str, username:str, password:str, options:dict=None) -> None:
        super(CareplusHpTarget, self).__init__(root_dir, options)
        self.root_dir = '/'
        self._start_url = 'https://edi.careplus-hp.com/edi/fileman.asp'
        self._session.auth = (username, password)
        self._response = None
        self._html = None

    def open(self, main_thread=False):
        self._response = self._session.get(self._start_url)
        if 400 >= self._response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= self._response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        else:
            self._html = lxml.html.fromstring(self._response.content)
            self.connected = True
        return self

    def _lstat(self, size:str, date:str) -> os.stat_result:
        mtime = ctime = calendar.timegm(time.strptime(date, "%m/%d/%Y %I:%M:%S %p"))
        size = size.replace(',', '')

        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            size,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def get_object(self, key: str)->CrawlFileObject:
        """
        :param key:str object key (without root_dir prefix)
        :return:
        """
        for obj in zip(
                self._html.xpath('//table[2]/tr[2]/td/table/tr[position()>1]/td[2]//text()'), #file name
                self._html.xpath('//table[2]/tr[2]/td/table/tr[position()>1]/td[2]//@href'), # url
                self._html.xpath('//table[2]/tr[2]/td/table/tr[position()>1]/td[3]//text()'), # size bytes (311,916,441)
                self._html.xpath('//table[2]/tr[2]/td/table/tr[position()>1]/td[5]//text()'), # date ('7/25/2017 7:04:53 AM')
        ):
            name, url, size, date = [el.strip() for el in obj]
            if name == key:
                return CrawlFileObject(
                    key=name,
                    stat=self._lstat(size, date),
                    link=urljoin(self._start_url, url),
                )

    def get_dir(self, dir_name: str) -> list:
        for key in self._html.xpath('//table[2]/tr[2]/td/table/tr[position()>1]/td[2]//text()'):
            yield self.get_object(key.strip())

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self.get_dir(self.root_dir)
