# -*- coding: utf-8 -*-
from synchronize.targets.crawlers.availity_base import AvailityBaseTarget
from synchronize.utils import get_first
from synchronize.objects import SelfDownloadableFileObject, DirectoryObject
from synchronize.utils import human2bytes
from datetime import datetime
from dateutil.parser import parse as date_parse
from itertools import zip_longest
from posixpath import join as posix_join
import calendar
import itertools
import logging
import lxml.html
import os
import re
import requests
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs


class AvailityHumanaTarget(AvailityBaseTarget):
    _FILE = 1
    _FOLDER = 2

    def __init__(self, *args, **kwargs) -> None:
        super(AvailityHumanaTarget, self).__init__(*args, **kwargs)
        # Override session
        self._session = requests.session()

    def _lstat(self, size, timestamp) -> os.stat_result:
        mtime = ctime = int(timestamp)
        size = size
        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            size,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _get_humana_dir(self, dir_name:str, event_target:str=None, view_state:str=None, view_state_generator:str=None,
                        path_selected:str=None, event_validation:str=None, request_url:str=None,
                        skip_files:bool=False):
        skip_select_provider = 1
        http_prefix = 'https://'
        event_argument = ''
        hf_bread_crumb = 'Availity'

        def get_item_type(i:str)->int:
            return self._FOLDER if i == 'Images/iconfolder.gif' else self._FILE

        def get_event_target(i:str)->str:
            # Example: "javascript:__doPostBack('ctl00$MainContent$rptFilesList$ctl02$linkFileName','')"
            m = re.search(r"\(([\w\$\,\'\"]+)\)", i)
            return m.group(1).split(',')[0].strip('\"\'')

        def get_view_state(html):
            return get_first(html.xpath('//*[@id="__VIEWSTATE"]/@value'), default='')

        def get_view_state_generator(html):
            return get_first(html.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value'))

        def get_event_validation(html):
            return get_first(html.xpath('//*[@id="__EVENTVALIDATION"]/@value'))

        def get_ftp_path(html):
            return get_first(html.xpath('//*[@id="PathSelected"]/@value'), default='')

        def get_next_url(html):
            return get_first(html.xpath('//*[@id="form1"]/@action'))

        def parse_size_field(i:str)->int:
            return human2bytes(i, symbols='humana')

        def parse_date_field(i:str)->str:
            d = date_parse(i)
            timestamp1 = calendar.timegm(d.timetuple())
            return datetime.utcfromtimestamp(timestamp1).strftime("%s")

        def download_file(item_link, item_name):
            response = self._session.post(
                request_url,
                data={
                    'skipSelectProvider': skip_select_provider,
                    '__EVENTARGUMENT': event_argument,
                    '__EVENTTARGET': item_link,
                    '__EVENTVALIDATION': event_validation,
                    '__VIEWSTATE': view_state,
                    '__VIEWSTATEGENERATOR': view_state_generator,
                    'ctl00$hfBreadCrumb': hf_bread_crumb,
                    'httpPrefix': http_prefix,
                },
                stream=True,
            )
            if response.status_code == 200:
                response.raw.decode_content = True
                return response.raw
            else:
                raise RuntimeWarning("Can't download file: %s, Link ID: %s, error: %s" % (
                    item_name, item_link, response.status_code))

        HUMANA_BASE_URL = 'https://apps.humana.com'
        if request_url is None:
            request_url = urljoin(HUMANA_BASE_URL, '/ServiceFundBBS/ServiceFundDirectories.aspx')

        if event_target is None or event_target == '':
            # This is root folder
            response = self._session.get(request_url)
        else:
            if path_selected == '':
                response = self._session.post(
                    request_url,
                    data={
                        'skipSelectProvider': skip_select_provider,
                        '__EVENTARGUMENT': event_argument,
                        '__EVENTTARGET': event_target,
                        '__EVENTVALIDATION': event_validation,
                        '__VIEWSTATE': view_state,
                        '__VIEWSTATEGENERATOR': view_state_generator,
                        'ctl00$hfBreadCrumb': hf_bread_crumb,
                        'httpPrefix': http_prefix,
                    }
                )
            else:
                # Documentation folder
                response = self._session.post(
                    request_url,
                    data={
                        'skipSelectProvider': skip_select_provider,
                        '__EVENTARGUMENT': event_argument,
                        '__EVENTTARGET': event_target,
                        '__EVENTVALIDATION': event_validation,
                        '__VIEWSTATE': view_state,
                        '__VIEWSTATEGENERATOR': view_state_generator,
                        'ctl00$hfBreadCrumb': hf_bread_crumb,
                        'httpPrefix': http_prefix,
                        'PathSelected': path_selected,
                    }
                )
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")

        html = lxml.html.fromstring(response.content)

        # Example: ftp://servicefund.humana.com:21/SF/Documentation/BBS%20and%20FTP
        # Optional argument
        cur_ftp_path = get_ftp_path(html)
        cur_path = dir_name

        curl_event_validation = get_event_validation(html)
        cur_view_state_generator = get_view_state_generator(html)
        cur_view_state = get_view_state(html)

        if len(html.xpath("//a[starts-with(@id, 'MainContent_rptFilesList_linkFileName')]/text()")) == 0:
            a = 1
        zipped_elements = sorted([(
            item_name, get_event_target(item_link), get_item_type(item_type),
            item_size and parse_size_field(item_size) or 0,
            item_date and parse_date_field(item_date) or 0) for item_name, item_link, item_type, item_size, item_date in zip_longest(
                html.xpath("//a[starts-with(@id, 'MainContent_rptFilesList_linkFileName')]/text()"),
                html.xpath("//a[starts-with(@id, 'MainContent_rptFilesList_linkFileName')]/@href"),
                html.xpath("//a[starts-with(@id, 'MainContent_rptFilesList_linkFileName')]/../img/@src"),
                html.xpath("//span[starts-with(@id, 'MainContent_rptFilesList_lblSize')]/text()"),
                html.xpath("//span[starts-with(@id, 'MainContent_rptFilesList_lblFileDateTime')]/text()"),
                fillvalue=None,
        )], key=lambda x: x[2])
        for item in zipped_elements:
            (item_name, item_link, item_type, item_size, item_date) = item

            if item_type == self._FOLDER:
                res_object = DirectoryObject(
                    key=posix_join(cur_path, item_name),
                    stat=self._lstat(size=item_size, timestamp=item_date)
                )
            else:
                res_object = SelfDownloadableFileObject(
                    key=posix_join(cur_path, item_name),
                    stat=self._lstat(size=item_size, timestamp=item_date),
                    file_object=download_file(item_link, posix_join(cur_path, item_name)),
                )
            yield res_object, {
                'name': item_name,
                'link': item_link,
                'type': item_type,
                '_cur_dir': cur_path,
                '_cur_ftp_path': cur_ftp_path,
                '_curl_event_validation': curl_event_validation,
                '_cur_view_state_generator': cur_view_state_generator,
                '_cur_view_state': cur_view_state,
                '_request_url': urljoin(HUMANA_BASE_URL, urljoin('/ServiceFundBBS/', get_next_url(html)))
            }

    def _get_humana(self):
        try:
            self._driver.switch_to.window(self._availity_home_tab)
            self._driver.get(self._availity_start_url)
            # Check that user is logged-in
            self._wait_for_class('user-account')

            self._driver.get(urljoin(self._start_url, '/availity/common/linkout_disclaimer.jsp?comavailityapps=%2Favaility%2Ftxn_993_994%2Fservice_fund.jsp%3FmenuTempl%3D1463'))

            # I Agree
            self._click_by_xpath("//input[@type='button' and @value='I Agree']")

            self._session = self._get_request_session_from_selenium()
        finally:
            self._driver.quit()
            self.connected = False

        processed_files = set()
        processed_directories = set()

        flag = True
        while flag:
            next_elements = self._get_humana_dir(dir_name='')
            candle_flag = True
            last_dir = ''
            while candle_flag:
                counter = 0
                files_counter = 0
                current, next_elements = itertools.tee(next_elements)
                intermediate = []
                for element_pair in current:
                    (el, metadata) = element_pair
                    logging.debug("Element: %s" % el.key)
                    if isinstance(el, DirectoryObject):
                        if metadata['_cur_dir'] in processed_directories or el.key in processed_directories:
                            pass
                        else:
                            counter += 1
                            intermediate.append(self._get_humana_dir(
                                dir_name=el.key,
                                event_target=metadata['link'],
                                view_state=metadata['_cur_view_state'],
                                view_state_generator=metadata['_cur_view_state_generator'],
                                path_selected=metadata['_cur_ftp_path'],
                                event_validation=metadata['_curl_event_validation'],
                                request_url=metadata['_request_url'],
                            ))
                            last_dir = el.key
                            break
                    else:
                        if metadata['_cur_dir'] not in processed_files:
                            files_counter +=1
                            yield el

                if counter == 0:
                    processed_directories.update({last_dir})
                    processed_files.update({last_dir})

                if files_counter > 0:
                    processed_files.update({metadata['_cur_dir']})

                next_elements = itertools.chain.from_iterable(intermediate)
                logging.debug("Iterations: %s" % counter)
                if counter == 0:
                    candle_flag = False

            if '' in processed_directories:
                flag = False

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self._get_humana()

    def open_readable(self, file_object:SelfDownloadableFileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, SelfDownloadableFileObject)
        return file_object.file_object
