# -*- coding: utf-8 -*-
__all__ = {'CareplusHpTarget', 'WellCareTarget', 'CoventryTarget'}
from synchronize.targets.crawlers.careplus_hp import CareplusHpTarget
from synchronize.targets.crawlers.wellcare import WellCareTarget
from synchronize.targets.crawlers.coventry import CoventryTarget
from synchronize.targets.crawlers.molina_fes import MolinaFESTarget
from synchronize.targets.crawlers.accureports import AccuReportsGenericTarget, ProviderAccuReportsTarget
from synchronize.targets.crawlers.freedomhealth import FreedomHealthTarget