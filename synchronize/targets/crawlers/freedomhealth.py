# -*- coding: utf-8 -*-
from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import SelfDownloadableFileObject
from synchronize.utils import get_first, noramlize_file_name
from tempfile import SpooledTemporaryFile
from posixpath import join as posix_join
import lxml.etree
import lxml.html
import os
import random
import time
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs
__all__ = {'FreedomHealthTarget'}


class FreedomHealthTarget(BaseCrawlerTarget):
    HOST = 'apps.freedomhealth.com'

    def __init__(self, root_dir: str, username: str, password: str, options: dict=None) -> None:
        super(FreedomHealthTarget, self).__init__(root_dir, options)
        self.root_dir = '/'
        self._start_url = urlunparse(('https', self.HOST, '/Login/Login.aspx', None, None, None))
        self._username = username
        self._password = password
        self._session.headers.update(
            {
                'Host': self.HOST,
                'Origin': urlunparse(('https', self.HOST, '', None, None, None)),
            }
        )

    def open(self, main_thread=False):
        response = self._get_page(self._start_url, expected_url=self._start_url)
        view_state = self._get_view_state(response.content)
        view_state_generator = self._get_view_state_generator(response.content)
        event_validation = self._get_event_validation(response.content)
        sm_main_hidden_field = self._get_sm_main_hidden_field(response.content)
        response = self._session.post(
            self._start_url,
            data={
                'smMain_HiddenField': sm_main_hidden_field,
                '__EVENTTARGET': '',
                '__EVENTARGUMENT': '',
                '__VIEWSTATE': view_state,
                '__VIEWSTATEGENERATOR': view_state_generator,
                '__EVENTVALIDATION': event_validation,
                'Login1$UserName': self._username,
                'Login1$Password': self._password,
                'Login1$LoginButton.x': random.choice(range(0, 60)),
                'Login1$LoginButton.y': random.choice(range(0, 20)),
            }
        )
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        else:
            if response.url == urljoin(self._start_url, '/Home/Home.aspx'):
                connected = True
            else:
                raise RuntimeError('Login failed')
        return self

    def _report_key_measures_equired(self):
        dir_name = 'KeyMeasuresRequired'
        url = urljoin(self._start_url, '/HCC_MEM_SNAP_SHOT/ShowReport_KeyMeasuresRequired.aspx')
        response = self._get_page(url, expected_url=url)
        # report
        view_state = self._get_view_state(response.content)
        view_state_generator = self._get_view_state_generator(response.content)
        event_validation = self._get_event_validation(response.content)
        response = self._session.post(
            url,
            data={
                'ctl00_ToolkitScriptManager1_HiddenField': '',
                '__EVENTTARGET': '',
                '__EVENTARGUMENT': '',
                '__VIEWSTATE': view_state,
                '__VIEWSTATEGENERATOR': view_state_generator,
                '__EVENTVALIDATION': event_validation,
                'ctl00$ContentPlaceHolder1$cbxPCPList$0': 'on',
                'ctl00$ContentPlaceHolder1$cbxPCPList$1': 'on',
                'ctl00$ContentPlaceHolder1$cbxPCPList$2': 'on',
                'ctl00$ContentPlaceHolder1$btnLoadReport': 'View Report',
                'ctl00$ContentPlaceHolder1$hdnCheckAllStatus': 'all',
            }
        )

        # export to excel
        view_state = self._get_view_state(response.content)
        view_state_generator = self._get_view_state_generator(response.content)
        event_validation = self._get_event_validation(response.content)
        event_target = 'ctl00$ContentPlaceHolder1$lbExportToExcel'  # self._get_event_target(response.content)
        response = self._session.post(
            url,
            data={
                'ctl00_ToolkitScriptManager1_HiddenField': '',
                'ctl00$ContentPlaceHolder1$cbxPCPList$0': 'on',
                'ctl00$ContentPlaceHolder1$cbxPCPList$1': 'on',
                'ctl00$ContentPlaceHolder1$cbxPCPList$2': 'on',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl03$ctl00': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl03$ctl01': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl10': 'ltr',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl11': 'standards',
                'ctl00$ContentPlaceHolder1$ReportViewer1$AsyncWait$HiddenCancelField': 'False',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ToggleParam$store': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ToggleParam$collapse': 'false',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl05$ctl00$CurrentPage': '1',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl05$ctl03$ctl00': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl08$ClientClickedId': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl07$store': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl07$collapse': 'false',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl09$VisibilityState$ctl00': 'ReportPage',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl09$ScrollPosition': '0 0',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl09$ReportControl$ctl02': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl09$ReportControl$ctl03': '',
                'ctl00$ContentPlaceHolder1$ReportViewer1$ctl09$ReportControl$ctl04': '100',
                'ctl00$ContentPlaceHolder1$hdnCheckAllStatus': 'none',
                '__EVENTTARGET': event_target,
                '__EVENTARGUMENT': '',
                '__VIEWSTATE': view_state,
                '__VIEWSTATEGENERATOR': view_state_generator,
                '__EVENTVALIDATION': event_validation,
            }
        )
        if response.status_code == 200 and response.headers.get('Content-Type') == 'application/csv' and response.headers.get('Content-Disposition'):
            file_name = self._get_content_disposition_filename(response.headers.get('Content-Disposition'))
            out = SpooledTemporaryFile(4 * 1024000)
            out.write(response.content)
            out.flush()
            out.seek(0)
            yield SelfDownloadableFileObject(
                key=posix_join(dir_name, file_name),
                stat=self._lstat(),
                file_object=out,
            )

    def walk(self, pred=None):
        yield from self._report_key_measures_equired()

    def open_readable(self, file_object:SelfDownloadableFileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, SelfDownloadableFileObject)
        return file_object.file_object

    def _lstat(self) -> os.stat_result:
        mtime = ctime = int(time.time())
        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            0,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _get_page(self, url, expected_url=None):
        response = self._session.get(url)
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        if expected_url and response.url != expected_url:
            raise RuntimeError("Error: We were expecting to get different URL: %s, but got: %s" % (
                expected_url, response.url))
        return response

    @staticmethod
    def _get_view_state(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//*[@id="__VIEWSTATE"]/@value'))

    @staticmethod
    def _get_view_state_generator(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value'))

    @staticmethod
    def _get_event_validation(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//*[@id="__EVENTVALIDATION"]/@value'))

    @staticmethod
    def _get_sm_main_hidden_field(html):
        html = lxml.html.fromstring(html)
        qs = parse_qs(get_first([s for s in html.xpath('//script/@src') if
                                 s.startswith('/Login/Login.aspx?_TSM_HiddenField_=smMain_HiddenField')]))
        return get_first(qs['_TSM_CombinedScripts_'])

    @staticmethod
    def _get_event_target(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//*[@id="__EVENTTARGET"]/@value'))

    @staticmethod
    def _get_content_disposition_filename(h):
        return h if h is None else noramlize_file_name(
            get_first([param.split('=')[1].strip('"\'') for param in h.split(';') if param.startswith('filename=')])
        )

