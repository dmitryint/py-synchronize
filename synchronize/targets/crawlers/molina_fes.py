from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import CrawlFileObject
from synchronize.utils import get_first
from posixpath import join as posix_join
import calendar
import datetime
import logging
import lxml.etree
import lxml.html
import os
import time
try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs
__all__ = {'MolinaFESTarget'}


class MolinaFESTarget(BaseCrawlerTarget):
    def __init__(self, root_dir: str, username: str, password: str, options: dict=None) -> None:
        super(MolinaFESTarget, self).__init__(root_dir, options)
        self._start_url = 'https://fes.molinahealthcare.com/FES/Login'
        self.root_dir = '/'
        self._username = username
        self._password = password

    def open(self, main_thread=False):
        response = self._get_page(self._start_url, expected_url=self._start_url)
        verification_token = self._get_verification_token(response.content)

        response_url = urljoin(self._start_url, '/FES/MyProfile')

        response = self._session.post(
            self._start_url,
            data={
                '__RequestVerificationToken': verification_token,
                'UserName': self._username,
                'Password': self._password,
                'btnLogin': 'Login',
            }
        )
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        else:
            response = self._get_page(response_url, expected_url=response_url)
            if self._is_logged(response.content):
                self.connected = True
            else:
                raise RuntimeError('Login failed')
        return self

    def _get_page(self, url, expected_url=None):
        response = self._session.get(url)
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        if expected_url and response.url != expected_url:
            raise RuntimeError("Error: We were expecting to get different URL: %s, but got: %s" % (expected_url, response.url))
        return response

    @staticmethod
    def _get_verification_token(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//input[@name="__RequestVerificationToken"]/@value'))

    @staticmethod
    def _get_receiver_trading_partner_id(html):
        html = lxml.html.fromstring(html)
        return get_first(html.xpath('//input[@name="ReceiverTradingPartnerID"]/@value'))

    @staticmethod
    def _is_logged(html):
        html = lxml.html.fromstring(html)
        return not any(html.xpath('//*[@id="FESLogInForm"]'))

    @staticmethod
    def _get_error_massage(self, html):
        pass

    @staticmethod
    def _parse_date(date: str):
        return time.strptime("%s" % date, "%m/%d/%Y %I:%M:%S %p")

    def _lstat(self, date: str) -> os.stat_result:
        """
        date:str 7/15/2017 7:46:22 PM
        """
        mtime = ctime = calendar.timegm(self._parse_date(date))

        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            0,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _section_download_file(self, from_date=None, to_date=None, file_category='', format_name=''):
        MAX_ITEMS_PER_REQEST = 250
        dir_name = 'DownloadFile'
        default_delta = 180
        section_url = urljoin(self._start_url, '/FES/DownloadFile')

        to_date = to_date and datetime.datetime.strptime(
            "%s 00:00" % to_date, "%m/%d/%Y %H:%M").date() or datetime.datetime.today().date()
        from_date = from_date and datetime.datetime.strptime(
            "%s 00:00" % from_date, "%m/%d/%Y %H:%M").date() or to_date - datetime.timedelta(days=default_delta)
        assert from_date <= to_date <= datetime.datetime.today().date()
        logging.debug('Processing request: %s (from: %s to: %s) MAX_ITEMS_PER_REQEST: %s' % (
            dir_name,
            from_date,
            to_date,
            MAX_ITEMS_PER_REQEST,
        ))

        flag = True
        intersecting_elements = []
        while flag:
            # prepare
            response = self._get_page(section_url, expected_url=section_url)

            partner_id = self._get_receiver_trading_partner_id(response.content)
            verification_token = self._get_verification_token(response.content)

            response = self._session.post(
                section_url,
                data={
                    '__RequestVerificationToken': verification_token,
                    'ReceiverTradingPartnerID': partner_id,
                    'FileCategoryValue': file_category,
                    'FormatName': format_name,
                    'EffectiveFromDate': from_date.strftime("%m/%d/%Y"),
                    'EffectiveToDate': to_date.strftime("%m/%d/%Y"),
                    'search': 'Search',
                }
            )
            if 400 >= response.status_code >= 499:
                raise RuntimeError("4×× Client Error")
            elif 500 >= response.status_code >= 599:
                raise RuntimeError("5×× Server Error")
            if section_url and response.url != section_url:
                raise RuntimeError("Error: We were expecting to get different URL: %s, but got: %s" % (
                    section_url, response.url))

            html = lxml.html.fromstring(response.content)

            counter = 0
            processing_date = None
            elements_at_cur_date = []
            for el in zip(
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[1]/a/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[1]/a/@href'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[2]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[3]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[3]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[5]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[6]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[7]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[8]/text()'),
                                html.xpath('//*[@id="dgDownload"]/tbody/tr/td[9]/a/@id'),
                        ):
                file_name, link, file_format, version, sending_trading_partner_id, receiving_trading_partner_id, is_archived, sender_id, file_time, detail = [
                    i.strip() for i in el]
                if file_name not in intersecting_elements:
                    yield CrawlFileObject(
                        key=posix_join(dir_name, file_name),
                        link=urljoin(self._start_url, link),
                        stat=self._lstat(file_time),
                    )

                if processing_date and processing_date == datetime.date.fromtimestamp(time.mktime(self._parse_date(file_time))):
                    elements_at_cur_date.append(file_name)
                else:
                    processing_date = datetime.date.fromtimestamp(time.mktime(self._parse_date(file_time)))
                    elements_at_cur_date = [file_name]
                counter += 1

            if counter < MAX_ITEMS_PER_REQEST:
                flag = False
                logging.debug('The request returned %s entries.' % counter)
            elif processing_date:
                logging.debug('The request (from: %s to: %s) returned more than 250 entries. We perform another onme (from: %s to: %s).' % (
                    from_date,
                    to_date,
                    processing_date,
                    to_date,
                ))
                intersecting_elements = elements_at_cur_date
                from_date = processing_date

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self._section_download_file(
            from_date=self.get_option('from_date'),
            to_date=self.get_option('to_date'),
        )
