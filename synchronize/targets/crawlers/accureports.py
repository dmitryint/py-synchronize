from synchronize.targets import BaseCrawlerTarget
from synchronize.objects import CrawlFileObject
from synchronize.utils import get_first
from posixpath import join as posix_join
import xml.etree.ElementTree as ET
import calendar
import os
import time

try:
    from urlparse import urlparse, urljoin, urlunparse, parse_qs
except:
    from urllib.parse import urlparse, urljoin, urlunparse, parse_qs
__all__ = {'AccuReportsGenericTarget', 'ProviderAccuReportsTarget'}


class AccuReportsGenericTarget(BaseCrawlerTarget):
    ACCU_HOST = None

    def __init__(self, root_dir: str, username: str, password: str, options: dict = None) -> None:
        super(AccuReportsGenericTarget, self).__init__(root_dir, options)
        self.root_dir = '/'

        self.index_url_pref = urlunparse(('https', self.ACCU_HOST, 'index.cfm', None, None, None))
        self.downloads_url_pref = urlunparse(('https', self.ACCU_HOST, 'downloads/', None, None, None))

        msg = self.get_option('msg', None)
        compid = self.get_option('compid', None)
        assert msg and compid
        parts = urlparse(self.index_url_pref)
        self._start_url = urlunparse((
            parts.scheme,
            parts.netloc,
            parts.path,
            parts.params,
            "compid=%s" % (compid),
            parts.fragment
        ))
        self._username = username
        self._password = password

    def open(self, main_thread=False):
        response = self._session.post(
            self._start_url,
            data={
                'username': self._username,
                'password': self._password,
                'login': '',
                'userwidth': 1280,
                'userheight': 800,
            }
        )
        if 400 >= response.status_code >= 499:
            raise RuntimeError("4×× Client Error")
        elif 500 >= response.status_code >= 599:
            raise RuntimeError("5×× Server Error")
        else:
            if urlparse(response.url).path == '/dash_ipa.cfm':
                self.connected = True
            else:
                raise RuntimeError('Login failed')
        return self

    def _lstat(self, size: str, date: str) -> os.stat_result:
        """
        date:str 2017-07-12
        size:str 10 (in megabytes)
        """
        mtime = ctime = calendar.timegm(time.strptime("%s 00:00:00" % date, "%Y-%m-%d %H:%M:%S"))

        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            int(size) * 1024 * 1024,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def _get_query_string_param(self, q, param):
        return get_first([get_first(v) for k, v in parse_qs(q).items() if k == param])

    def _get_downloads(self, url):
        url_parts = urlparse(url)
        batch_name = self._get_query_string_param(url_parts.query, 'batch_name')
        prefix = batch_name or ''
        compid = self.get_option('compid', None)

        response = self._session.get(url)
        if response.status_code == 200 and urlparse(response.url).path == url_parts.path:
            root = ET.fromstring(response.text)
            for el in root.iter('rowA'):
                url = el.find('URLFileName').text.strip()
                name = el.find('name').text.strip()
                size = el.find('size').text.strip()  # 15 (Mb)
                row_type = el.find('type').text.strip()  # File
                date = el.find('dateLastModified').text.strip()  # Jul 14, 17
                dateA = el.find('dateLastModifiedA').text.strip()  # 2017-07-14
                file_type = el.find('typeOfFile').text.strip()  # zip
                if row_type == 'File':
                    yield CrawlFileObject(
                        key=posix_join(prefix, name),
                        stat=self._lstat(size, dateA),
                        link=urljoin(
                            self.downloads_url_pref,
                            posix_join(compid, prefix, url)
                        ),
                    )
                else:
                    raise NotImplementedError
        else:
            raise RuntimeError("Code: %s Error while opening url: %s" % (response.status_code, url))

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self._get_downloads(urlunparse(('https', self.ACCU_HOST, 'includes/rpt_data/downloads_all_xml.cfm', None, None, None)))
        yield from self._get_downloads(
            urlunparse(('https', self.ACCU_HOST, 'includes/rpt_data/downloads_all_payorgroup_xml.cfm', None, 'batch_name=%s' % self._username, None))
        )


class ProviderAccuReportsTarget(AccuReportsGenericTarget):
    ACCU_HOST = 'provider.accureports.com'
