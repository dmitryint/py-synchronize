# -*- coding: iso-8859-1 -*-
from synchronize.const import DEFAULT_BLOCKSIZE
from synchronize.objects import FileObject, DirectoryObject, GenericObject
from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
from tempfile import SpooledTemporaryFile
from posixpath import join as posix_join, dirname as posix_dirname, basename as posix_basename, split as posix_split, relpath as posix_relpath
import logging
import paramiko
import stat as mod_stat
import pylru
__all__ = {'SftpTarget', 'FallbackSftpTarget'}


class SftpTarget(BaseTarget):
    def __init__(self, root_dir: str, host:str, port:int=22, username:str=None, password:str=None,
                 timeout: int=None, options: dict=None, read_only: bool=False):
        super(SftpTarget, self).__init__(root_dir, options, read_only)
        self._get_conn = self._get_sftp_connection(
            root_dir=root_dir,
            host=host,
            port=port,
            username=username,
            password=password,
            timeout=timeout
        )
        self._host = host
        self._transport = None
        self._cache = pylru.lrucache(1000)

    def __str__(self) -> str:
        return "<SFTP:%s>" % self.get_base_name()

    def __repr__(self) -> str:
        return "<SFTP:%s>" % self.get_base_name()

    def _get_sftp_connection(self, root_dir: str, host:str, port:int=22, username:str=None, password:str=None, timeout:int=None):
        def inner_func():
            self._transport = paramiko.Transport((host, port))
            hostkey = None
            self._transport.connect(
                hostkey,
                username,
                password,
            )
            return paramiko.SFTPClient.from_transport(self._transport)

        return inner_func

    def _is_exist(self, path: str) ->bool:
        try:
            s = self._sftp.stat(path)
        except FileNotFoundError as e:
            return False
        else:
            return s

    def get_base_name(self) -> str:
        scheme = "sftp"
        return "%s://%s%s" % (scheme, self._host, self.root_dir)

    def open(self, main_thread=False):
        self._sftp = self._get_conn()
        # TODO: handle sftp cwd
        # current dir always should be specified in config
        # ConnectionError: ('Connection aborted.', FileNotFoundError(2, 'No such file'))
        self.sftp_socket_connected = True
        self.connected = True
        return self

    def close(self):
        if isinstance(self._transport, paramiko.Transport) and self._transport.is_active():
            try:
                self._transport.close()
            except:
                logging.warning("Connection was closed due time out. But we did all the work.")
            finally:
                self.connected = False


    def get_id(self):
        return self._host + self.root_dir

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        dir_name = self._normpath(dir_name)
        flag = True
        dir = dir_name
        back_list = []
        while flag:
            try:
                if not self._is_exist(dir):
                    self._sftp.mkdir(dir)
            except FileNotFoundError:
                dir, back = posix_split(dir.rstrip('/'))
                if back != '':
                    back_list.insert(0, back)
            else:
                flag = False
        for el in range(len(back_list)):
            self._sftp.mkdir(posix_join(
                dir,
                *back_list[:el + 1],
            ))

    @do_not_run_if_dry
    def rmdir(self, dir_name:str) -> None:
        """Remove directory."""
        raise NotImplementedError

    def get_dir(self, dir_name:str) -> list:
        norm_dir_name = self._normpath(dir_name)
        try:
            for name, s in zip(self._sftp.listdir(norm_dir_name), self._sftp.listdir_attr(norm_dir_name)):
                path = posix_join(norm_dir_name, name)
                # Get the relative path from the self.root_dir
                key = posix_relpath(path, self.root_dir)
                yield self.get_object(key, s)
        except FileNotFoundError as e:
            logging.error("dir name: %s" % norm_dir_name)
            logging.error(e)
            raise

    def _lstat(self, key):
        path = self._normpath(key)
        return self._sftp.stat(path)

    def _is_file(self, key:str, s:paramiko.sftp_attr.SFTPAttributes=None) -> bool:
        if s and not mod_stat.S_ISDIR(s.st_mode):
            return True
        elif s is None:
            try:
                return self._is_file(key, s=self._lstat(key))
            except FileNotFoundError:
                return False
        else:
            return False

    def is_dir(self, key:str, s:paramiko.sftp_attr.SFTPAttributes=None) -> bool:
        if s and mod_stat.S_ISDIR(s.st_mode):
            return True
        elif s is None:
            try:
                return self._is_file(key, s=self._lstat(key))
            except FileNotFoundError:
                return False
        else:
            return False

    def get_object(self, key:str, s:paramiko.sftp_attr.SFTPAttributes=None) -> GenericObject:
        path = self._normpath(key)
        res = None
        if self._is_file(path, s):
            res = FileObject(
                key=posix_relpath(path, self.root_dir),
                stat=self._lstat(path)
            )
        elif self.is_dir(path, s):
            res = DirectoryObject(
                key=posix_relpath(path, self.root_dir),
                stat=self._lstat(path)
            )
        return res

    def open_readable(self, file_object:FileObject):
        assert isinstance(file_object, FileObject)
        return self._sftp.open(self._normpath(file_object.key))

    @do_not_run_if_dry
    def write_file(self, file_object:FileObject, fp_src, blocksize=DEFAULT_BLOCKSIZE, callback=None):
        assert isinstance(file_object, FileObject)
        dir_name = posix_dirname(self._normpath(file_object.dst_key or file_object.key))
        file_name = posix_basename(file_object.dst_key or file_object.key)
        self.mkdir(dir_name)
        self._sftp.putfo(fp_src, posix_join(dir_name, file_name))

    @do_not_run_if_dry
    def remove_file(self, file_object:FileObject):
        """Remove file."""
        assert isinstance(file_object, FileObject)
        self._sftp.remove(self._normpath(file_object.key))


class FallbackSftpTarget(SftpTarget):
    def open_readable(self, file_object: FileObject):
        assert isinstance(file_object, FileObject)
        out = SpooledTemporaryFile(4 * 1024000) #  max size before moving to file = 4MB
        self._sftp.getfo(self._normpath(file_object.key), out)
        out.flush()
        out.seek(0)
        return out
