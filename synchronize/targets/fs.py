# -*- coding: iso-8859-1 -*-
from synchronize.const import DEFAULT_BLOCKSIZE
from synchronize.objects import FileObject, DirectoryObject
from synchronize.utils import mkdir_p
from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
from posixpath import join as posix_join
import errno
import os
import shutil
__all__ = {'FsTarget'}


class FsTarget(BaseTarget):
    def __init__(self, root_dir: str, options: dict=None, read_only: bool=False) -> None:
        super(FsTarget, self).__init__(root_dir, options, read_only)

    def __str__(self):
        return "<FS:%s>" % self.root_dir

    def __repr__(self):
        return "<FS:%s>" % self.root_dir

    def open(self, main_thread=False):
        self.connected = True
        return self

    def get_object(self, key: str):
        path = self._normpath(key)
        res = None
        if os.path.isfile(path):
            res = FileObject(
                os.path.relpath(path, self.root_dir),
                os.lstat(path)
            )
        elif os.path.isdir(path):
            res = DirectoryObject(
                os.path.relpath(path, self.root_dir),
                os.lstat(path)
            )
        return res

    def _mkdir_p(self, path: str) -> None:
        mkdir_p(path)

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        self._mkdir_p(self._normpath(dir_name))

    @do_not_run_if_dry
    def rmdir(self, dir_name: str) -> None:
        """Remove cur_dir/name."""
        path = self._normpath(dir_name)
        shutil.rmtree(path)

    def get_dir(self, dir_name: str) -> list:
        for el in os.listdir(self._normpath(dir_name)):
            path = posix_join(self._normpath(dir_name), el)
            # Get the relative path from the self.root_dir
            key = os.path.relpath(path, self.root_dir)
            yield self.get_object(key)
        return

    def open_readable(self, file_object:FileObject):
        assert isinstance(file_object, FileObject)
        fp = open(self._normpath(file_object.key), "rb")
        return fp

    @do_not_run_if_dry
    def write_file(self, file_object, fp_src, blocksize=DEFAULT_BLOCKSIZE, callback=None):
        assert isinstance(file_object, FileObject)
        if not os.path.exists(self._normpath(file_object.dirname)):
            self.mkdir(self._normpath(file_object.dirname))

        with open(self._normpath(file_object.dst_key or file_object.key), "wb") as fp_dst:
            while True:
                data = fp_src.read(blocksize)
                if data is None or not len(data):
                    break
                fp_dst.write(data)
                if callback:
                    callback(data)
        return

    @do_not_run_if_dry
    def remove_file(self, key):
        """Remove cur_dir/name."""
        os.remove(self._normpath(key))

