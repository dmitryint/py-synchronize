# -*- coding: utf-8 -*-

__all__ = {'FsTarget', 'BaseTarget', 'FtpTarget', 'SftpTarget', 'S3Target',
           'S3VersionedTarget', 'BaseCrawlerTarget', 'FallbackSftpTarget', 'ShareFileTarget'}
from synchronize.targets.fs import FsTarget
from synchronize.targets.base import BaseTarget
from synchronize.targets.ftp_target import FtpTarget
from synchronize.targets.sftp_target import SftpTarget, FallbackSftpTarget
from synchronize.targets.s3_target import S3Target, S3VersionedTarget
from synchronize.targets.crawler_target import BaseCrawlerTarget
from synchronize.targets.sharefile import ShareFileTarget

