from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
from synchronize.const import DEFAULT_BLOCKSIZE
from synchronize.objects import FileObject, GenericObject
from synchronize.vendor import sharefile
from posixpath import join as posix_join, dirname as posix_dirname, basename as posix_basename, sep as posix_sep, split as posix_split
__all__ = {'ShareFileTarget'}


# TODO: Implement all methods
class ShareFileTarget(BaseTarget):
    def __init__(self, root_dir:str, host:str, username:str, password:str, api_key:str, api_secret:str, root_dir_id:str,
                 options: dict=None, read_only: bool=False):
        super(ShareFileTarget, self).__init__(root_dir, options, read_only)
        self._host = host
        self._username = username
        self._password = password
        self._api_key = api_key
        self._api_secret = api_secret
        self._root_dir_id = root_dir_id
        self._token = None
        self._dir_cache = {}

    def open(self, main_thread=False):
        token = sharefile.authenticate(
            hostname=self._host,
            client_id=self._api_key,
            client_secret=self._api_secret,
            username=self._username,
            password=self._password,
        )
        if token:
            self.connected = True
            self._token = token
        else:
            raise RuntimeError('Authentication error')
        return self

    def _create_directory(self, dir_name:str):
        chain = []
        parent_id = self._root_dir_id
        for el in dir_name.split('/'):
            chain.append(el)
            if '/'.join(chain) in self._dir_cache:
                parent_id = self._dir_cache['/'.join(chain)]
                continue
            cur_dir_listing = sharefile.list_dir_by_id(
                token=self._token,
                item_id=parent_id,
            )
            dir_id = None
            for obj_id, obj_date, obj_name, obj_type, obj_size in cur_dir_listing:
                if obj_name == el and obj_type == sharefile.DIR:
                    dir_id = obj_id
                    break
            if dir_id:
                parent_id = dir_id
            else:
                parent_id = sharefile.create_folder(
                    token=self._token,
                    parent_id=parent_id,
                    name=el,
                    description=el,
                )
            self._dir_cache['/'.join(chain)] = ''
        return parent_id


    def get_id(self):
        return self._host + self.root_dir.lstrip('/')

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        raise NotImplementedError

    @do_not_run_if_dry
    def rmdir(self, dir_name:str) -> None:
        """Remove directory."""
        raise NotImplementedError

    def get_dir(self, dir_name:str) -> list:
        raise NotImplementedError

    def get_object(self, key:str) -> GenericObject:
        raise NotImplementedError

    def open_readable(self, file_object:FileObject):
        assert isinstance(file_object, FileObject)
        raise NotImplementedError

    @do_not_run_if_dry
    def write_file(self, file_object, fp_src, blocksize=DEFAULT_BLOCKSIZE, callback=None):
        """Write binary data from file-like to cur_dir/name."""
        assert isinstance(file_object, FileObject)
        dir_name = posix_dirname(self._normpath(file_object.dst_key or file_object.key)).strip('/')
        id = self._dir_cache.get(dir_name) or self._create_directory(dir_name)

        sharefile.upload_file(
            token=self._token,
            folder_id=id,
            file_object=fp_src,
            file_name=posix_basename(file_object.dst_key or file_object.key),
            file_size=file_object.size,
        )

    @do_not_run_if_dry
    def remove_file(self, file_object:FileObject):
        """Remove file."""
        assert isinstance(file_object, FileObject)
        raise NotImplementedError
