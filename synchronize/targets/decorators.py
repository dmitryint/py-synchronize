def do_not_run_if_dry(function):
    def outer(cls, *args, **kwargs):
        return_value = None
        if not cls.dry_run:
            return_value = function(cls, *args, **kwargs)
        return return_value

    return outer
