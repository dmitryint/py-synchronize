# -*- coding: iso-8859-1 -*-
from synchronize.objects import CrawlFileObject, DirectoryObject
from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
import requests
try:
    from urlparse import urlparse
except:
    from urllib.parse import urlparse
__all__ = {'BaseCrawlerTarget'}


class BaseCrawlerTarget(BaseTarget):
    def __init__(self, root_dir: str, options: dict=None) -> None:
        super(BaseCrawlerTarget, self).__init__(root_dir, options, read_only=True)
        self._session = requests.Session()
        self._session.headers.update(
            {
                'User-Agent': 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0; Touch; MALNJS)'
            }
        )
        self._start_url = None

    def __str__(self) -> str:
        return "<Crawler:%s>" % self.get_base_name()

    def __repr__(self) -> str:
        return "<Crawler:%s>" % self.get_base_name()

    def get_base_name(self) -> str:
        if self._start_url:
            parts = urlparse(self._start_url)
            return "%s://%s" % (parts.scheme, parts.hostname)
        else:
            return None

    def open(self, main_thread=False):
        self.connected = True
        return self

    def close(self):
        self._session.close()
        self.connected = False

    def get_object(self, key: str):
        raise NotImplementedError

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        assert self.read_only

    @do_not_run_if_dry
    def rmdir(self, dir_name: str) -> None:
        assert self.read_only

    def get_dir(self, dir_name: str) -> list:
        raise NotImplementedError

    def open_readable(self, file_object:CrawlFileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, CrawlFileObject)
        r = self._session.get(file_object.link, stream=True)
        if r.status_code == 200:
            r.raw.decode_content = True
            return r.raw
        else:
            raise RuntimeWarning("Can't download file: %s, url: %s, error: %s" % (
                file_object.key, file_object.link, r.status_code))

    @do_not_run_if_dry
    def write_file(self, file_object, fp_src, blocksize=8192, callback=None):
        assert self.read_only

    @do_not_run_if_dry
    def remove_file(self, file_object: CrawlFileObject) -> None:
        assert self.read_only
