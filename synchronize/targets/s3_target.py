# -*- coding: iso-8859-1 -*-
from synchronize.const import DEFAULT_BLOCKSIZE
from synchronize.objects import FileObject, DirectoryObject
from synchronize.utils import get_first
from synchronize.targets.base import BaseTarget
from synchronize.targets.decorators import do_not_run_if_dry
from contextlib import ContextDecorator
import boto3
import botocore
import calendar
import io
import os
import time
import logging
__all__ = {'S3Target', 'S3VersionedTarget'}


class ReadableContext(ContextDecorator):
    def __init__(self, obj):
        self._obj = obj

    def __enter__(self):
        return self._obj

    def __exit__(self, *exc):
        return False


class S3ObjectInterator(io.RawIOBase):
    def __init__(self, file_object):
        self.obj_stream = file_object

    def readable(self):
        return True

    def read(self, n=DEFAULT_BLOCKSIZE):
        n = None if n < 0 else n
        return self.obj_stream.read(n)


class S3Target(BaseTarget):
    def __init__(self, root_dir:str, bucket:str, access_key_id:str=None, secret_access_key:str=None,
                 kms_key_id:str=None, region:str="us-east-1", timeout:int=60, wait_for_object=False,
                 options: dict=None, read_only: bool=False):
        super(S3Target, self).__init__(root_dir, options, read_only)
        self._s3 = None
        self._bucket = bucket
        self._access_key_id = access_key_id
        self._secret_access_key = secret_access_key
        self._kms_key_id = kms_key_id
        self._region = region
        self._timeout = timeout
        self.connected = False
        self._session = boto3.Session(
            aws_access_key_id=self._access_key_id,
            aws_secret_access_key=self._secret_access_key,
            region_name=self._region
        )
        self._wait_for_object = wait_for_object

    def __str__(self) -> str:
        return "<S3:%s>" % self.get_base_name()

    def __repr__(self) -> str:
        return "<S3:%s>" % self.get_base_name()

    def get_base_name(self) -> str:
        scheme = "s3"
        return self._access_key_id and "%s://%s@%s%s" % (
            scheme,
            self._access_key_id,
            self._bucket,
            self.root_dir
        ) or "%s://%s%s" % (scheme, self._bucket, self.root_dir)

    def open(self, main_thread=False):
        assert not self.connected
        # http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingAWSSDK.html#specify-signature-version
        s3 = self._s3 = self._session.client(
            's3',
            config=botocore.client.Config(signature_version='s3v4', read_timeout=self._timeout)
        )
        try:
            s3.get_object(Bucket=self._bucket, Key='this_key_does_not_exist')
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == 'NoSuchKey':
                # Successfully connected
                pass
            else:
                raise e
        self.connected = True
        return self

    def close(self):
        self.connected = False

    def get_id(self) -> str:
        return self.get_base_name()

    @do_not_run_if_dry
    def mkdir(self, dir_name: str) -> None:
        pass

    def _get_prefix_name(self, dir_name:str) -> str:
        prefix = self._normpath(dir_name).lstrip('/')
        return prefix if prefix.endswith('/') or len(prefix) == 0 else "%s/" % prefix

    def _check_errors(self, res:dict):
        if 'Errors' in res:
            raise RuntimeError(res['Errors'])

    @do_not_run_if_dry
    def rmdir(self, dir_name: str):
        pref = self._get_prefix_name(dir_name)
        objects = self._s3.list_objects_v2(Bucket=self._bucket, Prefix=pref)
        self._check_errors(objects)
        if objects['IsTruncated'] is True:
            raise NotImplementedError

        if objects['KeyCount'] > 0:
            self._check_errors(self._s3.delete_objects(
                Bucket=self._bucket,
                Delete = {
                    'Objects': [{'Key': el['Key']} for el in objects['Contents']]
                }
            ))

    def _lstat(self, el: dict) -> os.stat_result:
        mtime = ctime = calendar.timegm(el['LastModified'].timetuple()) if 'LastModified' in el else int(time.time())
        size = el.get('Size') if 'Size' in el else el['size']
        return os.stat_result((
            0,  # mode
            0,  # inode
            0,  # device
            1,  # hard links
            0,  # owner uid
            0,  # gid
            size,  # size
            0,  # atime
            mtime,  # mtime
            ctime,  # ctime
        ))

    def get_object_from_s3_event(self, s3_event):
        assert s3_event['bucket']['name'] == self._bucket

        return FileObject(
            key=s3_event['object']['key'],
            etag=s3_event['object']['eTag'],
            stat=self._lstat(s3_event['object']),
            version_id=s3_event['object']['versionId'],
            # The object received through the Lambda event is always the latest.
            is_latest=True,
        ) if 'versionId' in s3_event['object'] else FileObject(
            key=s3_event['object']['key'],
            etag=s3_event['object']['eTag'],
            stat=self._lstat(s3_event['object']),
        )

    def get_object(self, key: str, objects:dict=None, marker:str=None):
        """
        :param key:str object key (without root_dir prefix)
        :param objects:dict
        :param marker:str
        :return:
        """
        prefix = self._get_prefix_name(os.path.dirname(self._normpath(key)))
        # TODO: wrong
        s3_key = self._normpath(key).strip('/')
        #s3_key = key
        if marker is None:
            objects = objects or self._s3.list_objects_v2(
                Bucket=self._bucket,
                Prefix=prefix
            )
        else:
            objects = self._s3.list_objects_v2(Bucket=self._bucket, Prefix=prefix, ContinuationToken=marker)
        self._check_errors(objects)

        if objects['KeyCount'] > 0:
            match = get_first([el for el in objects['Contents'] if el['Key'].rstrip('/') == s3_key])
            if match and match['Key'].endswith('/'):
                return DirectoryObject(
                    key=key,
                    stat=self._lstat(match)
                )
            elif match:
                return FileObject(
                    key=key,
                    stat=self._lstat(match),
                    etag=match['ETag'],
                )
            if objects['IsTruncated'] is True:
                return self.get_object(key, marker=objects['NextContinuationToken'])

    def _s3_relpath(self, key):
        """
        Returns the path corresponding to the self.root_dir.
        :param key:
        :return:
        """
        return key.startswith(
            self._get_prefix_name(self.root_dir)
        ) and key[len(self._get_prefix_name(self.root_dir)):]

    def get_dir(self, dir_name: str, recursive:bool=False, objects:dict=None, marker=None) -> list:
        prefix = self._get_prefix_name(self._normpath(dir_name))
        if marker is None:
            objects = objects or self._s3.list_objects_v2(
                Bucket=self._bucket,
                Prefix=prefix
            )
        else:
            objects = self._s3.list_objects_v2(Bucket=self._bucket, Prefix=prefix, ContinuationToken=marker)

        if objects['KeyCount'] > 0:
            for el in objects['Contents']:
                d, f = os.path.split(el['Key'])
                if recursive:
                    yield self.get_object(self._s3_relpath(el['Key'].rstrip('/')), objects=objects)
                else:
                    if dir_name == d or (dir_name == os.path.dirname(d) and not f):
                        yield self.get_object(self._s3_relpath(el['Key'].rstrip('/')), objects=objects)
        if objects['IsTruncated'] is True:
            yield from self.get_dir(dir_name, recursive=recursive, marker=objects['NextContinuationToken'])

    def walk(self, pred=None):
        """Iterate over all target entries recursively."""
        yield from self.get_dir('', recursive=True)

    def open_readable(self, file_object:FileObject):
        """Return file-like object opened in binary mode for cur_dir/name."""
        assert isinstance(file_object, FileObject)
        if self._wait_for_object:
            pass
            """
            waiter = self._s3.get_waiter('object_exists')
            waiter.wait(
                Bucket=self._bucket,
                Key=self._normpath(file_object.key).lstrip('/'),
                VersionId=file_object.version,
                WaiterConfig={
                    'Delay': 30,
                }
            )
            """
        try:
            response = self._s3.get_object(
                Bucket = self._bucket,
                Key = self._normpath(file_object.key).lstrip('/'),
            )
        except botocore.exceptions.ClientError as ex:
            if ex.response['Error']['Code'] == 'NoSuchKey':
                logging.error('Key: %s Bucket: %s' % (self._normpath(file_object.key).lstrip('/'), self._bucket))
            raise
        #return ReadableContext(response['Body'])

        # TODO: change this
        # https://github.com/boto/botocore/issues/879
        return S3ObjectInterator(response['Body'])

        #return io.BufferedReader(S3ObjectInterator(response['Body']), buffer_size=DEFAULT_BLOCKSIZE*20)

    @do_not_run_if_dry
    def write_file(self, file_object, fp_src, blocksize=DEFAULT_BLOCKSIZE, callback=None):
        """Write binary data from file-like to cur_dir/name."""
        assert isinstance(file_object, FileObject)
        if self._kms_key_id:
            self._s3.upload_fileobj(
                fp_src,
                self._bucket,
                self._normpath(file_object.dst_key or file_object.key).lstrip('/'),
                ExtraArgs={
                    'ServerSideEncryption': 'aws:kms',
                    'SSEKMSKeyId': self._kms_key_id,
                }
            )
        else:
            self._s3.upload_fileobj(
                fp_src,
                self._bucket,
                self._normpath(file_object.dst_key or file_object.key).lstrip('/'),
            )

    @do_not_run_if_dry
    def remove_file(self, file_object: FileObject):
        assert isinstance(file_object, FileObject)
        self._s3.delete_object(
            Bucket=self._bucket,
            Key=file_object.key
        )


class S3CachedTarget(S3Target):
    def __init__(self, *args, **kwargs):
        super(S3CachedTarget, self).__init__(*args, **kwargs)
        self._cache = {}

    def open(self, main_thread=False):
        super(S3CachedTarget, self).open(main_thread)
        if main_thread:
            self._cache = {obj.key: obj for obj in self.walk()}

    def get_object(self, key: str, objects:dict=None, marker:str=None):
        if key in self._cache:
            logging.debug("Retrieving object from the cache: %s" % key)
            return self._cache[key]
        else:
            return super(S3CachedTarget, self).get_object(key, objects, marker)


class S3VersionedTarget(S3Target):
    def __init__(self, *args, **kwargs):
        super(S3VersionedTarget, self).__init__(*args, **kwargs)
        self._res = None

    def __str__(self) -> str:
        return "<S3V:%s>" % self.get_base_name()

    def __repr__(self) -> str:
        return "<S3V:%s>" % self.get_base_name()

    def get_object(self, key: str, objects:dict=None, version_id:str=None, version_marker:str=None, key_marker:str=None):
        """
        :param key:str object key (without root_dir prefix)
        :param objects:dict
        :param version_id:str version id. If None then return latest version.
        :param version_marker:str
        :param key_marker:str
        :return:
        """
        prefix = self._get_prefix_name(os.path.dirname(self._normpath(key)))
        # TODO: wrong
        s3_key = self._normpath(key).strip('/')
        #s3_key = key
        if version_marker is None:
            objects = objects or self._s3.list_object_versions(
                Bucket=self._bucket,
                Prefix=prefix,
            )
        else:
            objects = self._s3.list_object_versions(
                Bucket=self._bucket,
                Prefix=prefix,
                KeyMarker=key_marker,
                VersionIdMarker=version_marker,
            )
        self._check_errors(objects)

        match = get_first([el for el in objects['Versions']
                           if (el['Key'].rstrip('/') == s3_key) and (
                               (version_id and el['VersionId'] == version_id) or (
                                   version_id is None and el['IsLatest'] == True))])
        if match and match['Key'].endswith('/'):
            return DirectoryObject(
                key=key,
                stat=self._lstat(match)
            )
        elif match:
            return FileObject(
                key=key,
                is_latest=match['IsLatest'],
                version_id=match['VersionId'],
                etag=match['ETag'],
                stat=self._lstat(match),
            )
        if objects['IsTruncated'] is True:
            return self.get_object(
                key,
                version_marker=objects['NextVersionIdMarker'],
                key_marker = objects['NextKeyMarker'],
            )

    def get_dir(self, dir_name: str, recursive:bool=False, objects:dict=None, version_marker:str=None,
                           key_marker:str=None) -> list:
        prefix = self._get_prefix_name(self._normpath(dir_name))
        if version_marker is None:
            objects = objects or self._s3.list_object_versions(
                Bucket=self._bucket,
                Prefix=prefix,
            )
        else:
            objects = self._s3.list_object_versions(
                Bucket=self._bucket,
                Prefix=prefix,
                KeyMarker=key_marker,
                VersionIdMarker=version_marker,
            )

        for el in objects.get('Versions', []):
            d, f = os.path.split(el['Key'])
            if recursive or (dir_name == d or (dir_name == os.path.dirname(d) and not f)):
                yield self.get_object(
                    self._s3_relpath(el['Key'].rstrip('/')),
                    objects=objects,
                    version_id=el['VersionId'],
                )

        if objects['IsTruncated'] is True:
            yield from self.get_dir(
                dir_name,
                recursive=recursive,
                version_marker=objects['NextVersionIdMarker'],
                key_marker=objects['NextKeyMarker'],
            )
