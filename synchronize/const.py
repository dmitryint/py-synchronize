# -*- coding: iso-8859-1 -*-
import os
__all__ = {'DEFAULT_BLOCKSIZE'}

DEFAULT_BLOCKSIZE = int(os.environ.get('DEFAULT_BLOCKSIZE', 1024*1024*4))
AWS_METADATA_URL='http://169.254.169.254/latest/'

MODULE_PATH = os.path.dirname(os.path.abspath(__file__))
