# -*- coding: iso-8859-1 -*-
from __future__ import print_function
from datetime import datetime
from os.path import basename, dirname
from posixpath import join as posix_join, splitext as posix_splitext
import os
#import tzlocal
__all__ = {'DirectoryObject', 'FileObject', 'CrawlFileObject', 'SelfDownloadableFileObject', 'GenericObject'}


class GenericObject(object):
    def __init__(self, key: str, stat: os.stat_result) -> None:
        self._key = key
        self._stat = stat
        self._is_file = False
        self._is_dir = False

    def is_file(self):
        return self._is_file

    def is_dir(self):
        return self._is_dir

    def __gt__(self, obj2):
        return self.mtime > obj2.mtime

    @property
    def name(self) -> str:
        return basename(self._key)

    @property
    def dirname(self) -> str:
        return dirname(self._key)

    @property
    def key(self) -> str:
        return self._key

    @property
    def dst_key(self):
        return None

    @property
    def mtime(self) -> int:
        return self._stat.st_mtime

    @property
    def size(self):
        return self._stat.st_size


class FileObject(GenericObject):
    def __init__(self, key: str, stat: os.stat_result, is_latest=True, version_id:str=None, etag:str=None,
                 versions_dir='__historical__') -> None:
        """
        :param key:
        :param stat:
        :param is_latest: True | False or None (default: True)
        :param version_id:
        :param etag:
        :param versions_dir:
        """
        super(FileObject, self).__init__(key, stat)
        self._is_file = True
        self._version_id = version_id
        self._etag = etag
        self._is_latest = is_latest
        self._versions_dir = versions_dir

    def __str__(self):
        return "<File:%s>" % self._key

    def __repr__(self):
        return "<File:%s>" % self._key

    @property
    def name(self) -> str:
        if self._is_latest:
            return basename(self._key)
        else:
            unix_timestamp = float(self.mtime)
            #local_timezone = tzlocal.get_localzone()
            #local_time = datetime.fromtimestamp(unix_timestamp, local_timezone)
            utc_time = datetime.utcfromtimestamp(unix_timestamp)
            fname, ext = posix_splitext(basename(self._key))
            return "%s_%s%s" % (
                fname,
                utc_time.strftime("%Y-%m-%d_%H%M%S"),
                ext
            )

    @property
    def dirname(self) -> str:
        if self._is_latest:
            return dirname(self._key)
        else:
            return posix_join(dirname(self._key), self._versions_dir)

    @property
    def key(self) -> str:
        return self._key

    @property
    def dst_key(self):
        if self._is_latest:
            return self._key
        else:
            return posix_join(self.dirname, self.name)

    @property
    def version(self):
        return self._version_id

    @property
    def etag(self):
        return self._etag

    @property
    def is_latest(self):
        return self._is_latest


class DirectoryObject(GenericObject):
    def __init__(self, key: str, stat: os.stat_result) -> None:
        super(DirectoryObject, self).__init__(key, stat)
        self._is_dir = True

    def __str__(self):
        return "<Directory:%s>" % self._key

    def __repr__(self):
        return "<Directory:%s>" % self._key

    @property
    def name(self) -> str:
        raise RuntimeError

    @property
    def dirname(self) -> str:
        return self._key


class CrawlFileObject(FileObject):
    def __init__(self, key: str, stat: os.stat_result, link:str) -> None:
        super(CrawlFileObject, self).__init__(key, stat)
        self._link = link

    @property
    def link(self):
        return self._link


class SelfDownloadableFileObject(FileObject):
    def __init__(self, key: str, stat: os.stat_result, file_object, is_latest=True, version_id:str=None, etag:str=None,
                 versions_dir='__historical__') -> None:
        super(SelfDownloadableFileObject, self).__init__(
            key=key,
            stat=stat,
            is_latest=is_latest,
            version_id=version_id,
            etag=etag,
            versions_dir=versions_dir
        )
        self._file_object = file_object

    @property
    def file_object(self):
        return self._file_object

    def __str__(self):
        return "<DownloadedFile:%s>" % self._key

    def __repr__(self):
        return "<DownloadedFile:%s>" % self._key
