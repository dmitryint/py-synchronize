# -*- coding: iso-8859-1 -*-
from __future__ import print_function
from synchronize.utils import get_current_region
from pythonjsonlogger import jsonlogger
import boto3
import logging
import time
import watchtower


def init_logger(log_group=None, debug=False):
    stream_name=time.strftime("%d-%m-%Y_%H%M%S")

    def __inner():
        if debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        json_formatter = jsonlogger.JsonFormatter()
        #logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")

        logger = logging.getLogger()

        if log_group:
            boto3_session = boto3.session.Session(region_name=get_current_region())
            cloudWatchHandler = watchtower.CloudWatchLogHandler(
                log_group=log_group,
                stream_name=stream_name,
                boto3_session=boto3_session,
                create_log_group=False,
            )
            cloudWatchHandler.setFormatter(json_formatter)
            logger.addHandler(cloudWatchHandler)

        #consoleHandler = logging.StreamHandler()
        #consoleHandler.setFormatter(logFormatter)
        #logger.addHandler(consoleHandler)

        logging.getLogger('botocore').setLevel(logging.CRITICAL)
        logging.getLogger('boto3').setLevel(logging.CRITICAL)
        logging.getLogger('requests').setLevel(logging.CRITICAL)

    return __inner
