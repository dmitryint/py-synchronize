from synchronize.cli.helpers import make_target, unwrap_from_sns_message
import argparse
import boto3
import json
import yaml
import logging
import traceback
try:
    from urlparse import unquote_plus, urlunparse, urlparse
except:
    from urllib.parse import unquote_plus, urlunparse, urlparse, unquote


def _get_parser() ->argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Cloud synchronizer')
    parser.add_argument('-f', '--file', required=True, action='store', dest='file', help='yaml configuration')
    parser.add_argument('-d', '--debug', default=False, action='store', dest='debug', help='Debug')
    parser.add_argument('--dry', default=False, action='store', dest='dry', help='Dry run')
    return parser


def _get_config_from_file(bucket, config, prefix):
    for job in config['jobs']:
        if urlparse(job['source']['url'], allow_fragments=False).netloc == bucket:
            return job[prefix]


def get_source_config(bucket, config):
    return _get_config_from_file(
        bucket=bucket,
        config=config,
        prefix='source',
    )


def get_destination_config(bucket, config):
    return _get_config_from_file(
        bucket=bucket,
        config=config,
        prefix='destination',
    )


def create_pair_of_targets(bucket, config):
    make_s = make_target(
        urlunparse(('s3', bucket, '/', None, None, None)),
        extra_opts=get_source_config(bucket, config),
        read_only=True,
    )

    dst_extra_opts = get_destination_config(bucket, config)
    make_d = make_target(
        dst_extra_opts['url'],
        extra_opts=dst_extra_opts,
    )
    return {
        'source': make_s(),
        'destination': make_d(),
    }


def run():
    args = _get_parser().parse_args()
    dry_run = args.dry
    with open(args.file, 'r') as stream:
        doc = yaml.load(stream)

    queue_name = doc['queue_name']
    queue_region = doc['queue_region']

    # Choosing the resource from boto3 module
    sqs = boto3.resource('sqs', region_name=queue_region)

    # Get the queue named test
    queue = sqs.get_queue_by_name(QueueName=queue_name)

    def _sqs_receive_messages(q):
        while True:
            message_list = q.receive_messages()
            if message_list:
                for message in message_list:
                    yield message
            else:
                break

    processing_counter = 0
    errors_counter = 0
    for message in _sqs_receive_messages(queue):
        targets_pairs = {}
        processing_counter +=1
        try:
            event = json.loads(message.body)
            # event is SNS message
            m = unwrap_from_sns_message(event)
            for record in [record for s3_event_records in m for record in s3_event_records['Records'] if 'Records' in s3_event_records ]:
                assert record['eventSource'] == 'aws:s3'
                assert record['eventName'].startswith('ObjectCreated:')
                logging.warning("Processing message: %s" % record)

                bucket = record['s3']['bucket']['name']
                record['s3']['object']['key'] = key = unquote_plus(record['s3']['object']['key'])

                if key.endswith('/'):
                    """The key is directory object"""
                    pass
                else:
                    """The key is file object"""
                    try:
                        if bucket not in targets_pairs:
                            targets_pairs.update({bucket: create_pair_of_targets(bucket, config=doc)})

                        source = targets_pairs[bucket]['source'].open()
                        target = targets_pairs[bucket]['destination'].open()

                        new_file = source.get_object_from_s3_event(record['s3'])
                        with source.open_readable(new_file) as fp_src:
                            target.write_file(new_file, fp_src)
                    except Exception:
                        raise
                    finally:
                        source.close()
                        target.close()
        except Exception as e:
            errors_counter +=1
            logging.error("Exception: %s \n during processing a message: %s" % (e, event))
            traceback.print_exc()
        else:
            message.delete()

    logging.warning("Processed: %s Errors: %s" %(processing_counter, errors_counter))

if __name__ == "__main__":
    run()
