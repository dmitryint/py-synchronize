# -*- coding: iso-8859-1 -*-
from __future__ import print_function
from synchronize import Syncer
from synchronize.cli.helpers import make_target
from synchronize.objects import FileObject
from synchronize.logger import init_logger
from multiprocessing import Pool
import logging
import platform
import argparse
import yaml
import sys
import uuid
import threading
try:
    import thread
except ImportError:
    import _thread as thread

try:
    from urlparse import urlparse
except:
    from urllib.parse import urlparse

__all__ = {'run'}

EXIT_AFTER = 3600


def _get_parser() ->argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Cloud synchronizer')
    parser.add_argument('-o', '--only', action='store', dest='only', help='Run this task only')
    parser.add_argument('-f', '--file', required=True, action='store', dest='file', help='yaml configuration')
    parser.add_argument('-c', default=3, action='store', dest='concurrency', help='concurrency')
    parser.add_argument('-m', default=50, action='store', dest='maxtasksperchild', help='Max tasks per worker')
    parser.add_argument('-d', '--debug', default=False, action='store', dest='debug', help='Debug')
    parser.add_argument('--dry', default=False, action='store', dest='dry', help='Dry run')
    return parser


def init_worker(make_s_fun, make_d_fun, set_log_fun):
    global src, dest, worker_name, log
    set_log_fun()
    log = logging.getLogger()
    worker_name = str(uuid.uuid4())
    logging.info(
        "Initialzing worker: %s" % worker_name,
        extra={
            "worker_id": "%s" % worker_name,
        }
    )
    src=make_s_fun()
    dest=make_d_fun()
    src.open()
    dest.open()


def copy_file(blob:FileObject):
        assert isinstance(blob, FileObject)
        global src, dest, log, worker_name
        logging = log
        logging.info(
            'Copying file: %s from %s to %s' % (blob.key, src, dest),
            extra={
                "operation_type": "copy",
                "blob_name": "%s" % blob.key,
                "copy_from": "%s" % src,
                "copy_to": "%s" % dest,
                "worker_id": "%s" % worker_name,
            }
        )

        def __block_written(data):
            pass

        try:
            with src.open_readable(blob) as fp_src:
                dest.write_file(blob, fp_src)
        except RuntimeWarning as e:
            logging.exception(
                "Non Fatal Exception: %s" % e,
                extra={
                    "operation_type": "error",
                }
            )
        except Exception as e:
            logging.exception(
                "Exception: %s" % e,
                  extra={
                      "operation_type": "error",
                  }
            )
            logging.shutdown()
            raise


def _parse_extra(target):
    res = {}
    if "credentials" in target:
        res.update({"credentials": target['credentials']})
    if "encryption" in target:
        res.update({'encryption': target['encryption']})
    if "region" in target:
        res.update({'region': target['region']})
    if "aws_access_key_id" in target:
        res.update({'aws_access_key_id': target['aws_access_key_id']})
    if "aws_secret_access_key" in target:
        res.update({'aws_secret_access_key': target['aws_secret_access_key']})
    if "class" in target:
        res.update({'class': target['class']})
    if "compare_size" in target:
        res.update({'compare_size': bool(target['compare_size'])})
    if "single_process" in target:
        res.update({'single_process': bool(target['single_process'])})
    return res


def _run_job(job, args, set_log_fun, dry_run=False):
    logging.info("Starting job: %s" % job['name'])
    make_s = make_target(
        job['source']['url'],
        extra_opts=_parse_extra(job['source']),
        read_only=True,
        dry_run=dry_run,
    )
    make_d = make_target(
        job['destination']['url'],
        extra_opts=_parse_extra(job['destination']),
        dry_run=dry_run,
    )
    no_pool = True if _parse_extra(job['source']).get('single_process') or _parse_extra(job['destination']).get('single_process') else False
    if platform.system().lower() == 'windows':
        logging.info('Running on windows')
        s = Syncer(make_s(), make_d(), options={})
        s.copy()
    else:
        if no_pool:
            logging.info('Single process mode')
            s = Syncer(make_s(), make_d(), options={})
            s.copy()
        else:
            logging.info('Multi process mode. Concurrency: %s' % args.concurrency)
            with Pool(processes=int(args.concurrency), initializer=init_worker, initargs=[make_s, make_d, set_log_fun],
                    maxtasksperchild=int(args.maxtasksperchild)) as p:
                s = Syncer(make_s(), make_d(), pool=p, pool_func=copy_file, options={})
                s.copy()


def quit_function(fn_name):
    # print to stderr, unbuffered in Python 2.
    logging.error('{0} took too long'.format(fn_name))
    sys.stderr.flush() # Python 3 stderr is likely buffered.
    thread.interrupt_main() # raises KeyboardInterrupt


def exit_after(s):
    '''
    use as decorator to exit process if
    function takes longer than s seconds
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, quit_function, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer


@exit_after(EXIT_AFTER)
def run(dry_run=False):
    args = _get_parser().parse_args()
    dry_run = args.dry
    stream = open(args.file, 'r')
    doc = yaml.load(stream)
    for job in doc['jobs']:
        if (args.only and args.only == job['name']) or not args.only:
            cloudwatch_group_name = job['cloudwatch_group'] if 'cloudwatch_group' in job else None
            set_log = init_logger(cloudwatch_group_name, debug=args.debug)
            set_log()
            _run_job(job, args, set_log, dry_run=dry_run)


if __name__ == "__main__":
    run()
