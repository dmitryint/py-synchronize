# -*- coding: utf-8 -*-
from synchronize.cli.helpers import make_target, unwrap_from_sns_message
import os
try:
    from urlparse import unquote_plus, urlunparse
except:
    from urllib.parse import unquote_plus, urlunparse


def _get_config_from_env(prefix, key_transform_func):
    res = {}
    for el in list(os.environ.keys()):
        if el.startswith(prefix):
            _, key = el.split('_', 1)
            res.update({key_transform_func(key): os.environ.get(el)})
    return res


def get_source_config():
    return _get_config_from_env(
        prefix='SRC_',
        key_transform_func=lambda x: x.lower(),
    )


def get_destination_config():
    return _get_config_from_env(
        prefix='DST_',
        key_transform_func=lambda x: x.lower(),
    )


def get_mappings():
    return _get_config_from_env(
        prefix='MAP_',
        key_transform_func=lambda x: x.lower().replace('_', '-'),
    )


def create_pair_of_targets(bucket):
    make_s = make_target(
        urlunparse(('s3', bucket, '/', None, None, None)),
        extra_opts=get_source_config(),
        read_only=True,
    )

    dst_extra_opts = get_destination_config()
    dst_dir = get_mappings()[bucket]
    make_d = make_target(
        urlunparse((dst_extra_opts['schema'], dst_extra_opts['hostname'], dst_dir, None, None, None)),
        extra_opts=dst_extra_opts,
    )
    return {
        'source': make_s().open(),
        'destination': make_d().open(),
    }


def lambda_handler(event, context):
    targets_pairs = {}
    print(event)
    # event is SNS message
    for record in [record for s3_event_records in unwrap_from_sns_message(event) for record in s3_event_records['Records']]:
        assert record['eventSource'] == 'aws:s3'
        assert record['eventName'].startswith('ObjectCreated:')

        bucket = record['s3']['bucket']['name']
        record['s3']['object']['key'] = key = unquote_plus(record['s3']['object']['key'])

        if key.endswith('/'):
            """The key is directory object"""
            pass
        else:
            """The key is file object"""
            try:
                if bucket not in targets_pairs:
                    try:
                        targets_pairs.update({bucket: create_pair_of_targets(bucket)})
                    except KeyError:
                        print("The bucked: %s is skipped. Check the environment variables." % bucket)
                        raise

                source = targets_pairs[bucket]['source']
                target = targets_pairs[bucket]['destination']

                new_file = source.get_object_from_s3_event(record['s3'])
                with source.open_readable(new_file) as fp_src:
                    target.write_file(new_file, fp_src)
            except Exception:
                raise
            finally:
                source.close()
                target.close()
