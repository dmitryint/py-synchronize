from synchronize.utils import get_credentials, query_string_params_to_dict
import distutils.util
import synchronize.targets as targets
import logging
import json
try:
    from urlparse import urlparse
except:
    from urllib.parse import urlparse


def _get_class( kls ):
    parts = kls.split('.')
    module = ".".join(parts[:-1])
    m = __import__( module )
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m


def make_target(url, extra_opts=None, read_only=False, dry_run=False):
    def __inner():
        target = None
        parts = urlparse(url, allow_fragments=False)
        scheme = parts.scheme.lower()

        d = {
            'dry_run': dry_run,
        }

        if 'credentials' in extra_opts:
            login, password = get_credentials(extra_opts['credentials'])
        elif 'login' in extra_opts and 'password' in extra_opts:
            login = extra_opts['login']
            password = extra_opts['password']

        if scheme in ['ftp', 'ftps', 'iftps']:
            d.update(query_string_params_to_dict(parts.query))
            target = targets.FtpTarget(
                root_dir=parts.path,
                host=parts.hostname,
                port=parts.port,
                tls=True if scheme == 'ftps' else False,
                implict_tls=True if scheme == 'iftps' else False,
                passive=True,
                username=login,
                password=password,
                options=d,
            )
        elif scheme in ['sftp']:
            #d = {
            #    'pool_safe': False,
            #}
            if extra_opts.get('class'):
                logging.debug("Custom class: %s" % extra_opts.get('class'))
                tgt_cls = _get_class(extra_opts.get('class'))
            else:
                tgt_cls = targets.SftpTarget

            port = parts.port or 22
            target = tgt_cls(
                root_dir=parts.path,
                host=parts.hostname,
                port=port,
                username=login,
                password=password,
                options=d,
            )
        elif scheme in ['http', 'https']:
            if extra_opts.get('class'):
                d.update(query_string_params_to_dict(parts.query))
                if 'compare_size' in extra_opts:
                    d.update({'compare_size': extra_opts.get('compare_size')})
                tgt_cls = _get_class(extra_opts.get('class'))
                target = tgt_cls(
                    root_dir='/',
                    username=login,
                    password=password,
                    options=d,
                )
            else:
                raise RuntimeError("You must specify a class")
        elif scheme in ['sharefile']:
            api_key, api_secret = get_credentials(extra_opts['api_key'])
            if extra_opts.get('class'):
                logging.debug("Custom class: %s" % extra_opts.get('class'))
                tgt_cls = _get_class(extra_opts.get('class'))
            else:
                tgt_cls = targets.ShareFileTarget
            target = tgt_cls(
                root_dir=parts.path,
                host = parts.hostname,
                username = login,
                password = password,
                api_key = api_key,
                api_secret = api_secret,
                root_dir_id = extra_opts['root_dir_id'],
                options=d,
            )
        elif scheme in ['s3']:
            if extra_opts.get('class'):
                logging.debug("Custom class: %s" % extra_opts.get('class'))
                tgt_cls = _get_class(extra_opts.get('class'))
            else:
                tgt_cls = targets.S3Target
            target = tgt_cls(
                root_dir=parts.path,
                bucket=parts.hostname,
                kms_key_id=extra_opts.get('encryption'),
                access_key_id=extra_opts.get('aws_access_key_id'),
                secret_access_key=extra_opts.get('aws_secret_access_key'),
                region=extra_opts['region'],
                options=d,
                # Enable object pending feature
                # make sense for Lambda invoke
                wait_for_object = bool(distutils.util.strtobool(extra_opts.get('wait_for_object', 'False'))),
            )
        elif scheme in ['s3v']:
            if extra_opts.get('class'):
                logging.debug("Custom class: %s" % extra_opts.get('class'))
                tgt_cls = _get_class(extra_opts.get('class'))
            else:
                tgt_cls = targets.S3VersionedTarget
            target = tgt_cls(
                root_dir=parts.path,
                bucket=parts.hostname,
                kms_key_id=extra_opts.get('encryption'),
                access_key_id=extra_opts.get('aws_access_key_id'),
                secret_access_key=extra_opts.get('aws_secret_access_key'),
                region=extra_opts['region'],
                options=d,
            )
        elif scheme in ['file']:
            target = targets.FsTarget(
                root_dir=parts.path,
                options=d,
            )
        else:
            raise RuntimeError('Scheme: %s not implemented' % scheme)
        return target

    return __inner


def unwrap_from_sns_message(message):
    """
    Get aws:s3 event from SNS message
    """
    res = []
    for record in message['Records']:
        assert record['EventSource'] == 'aws:sns'
        res.append(json.loads(record['Sns']['Message']))
    return res
