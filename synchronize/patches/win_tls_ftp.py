# -*- coding: iso-8859-1 -*-
from synchronize.patches.implicit_ftps import IMPL_FTP_TLS
import ftplib
try:
    import ssl
except ImportError:
    _SSLSocket = None
else:
    _SSLSocket = ssl.SSLSocket
__all__={ 'FTP_TLS_WIN', 'IMPL_FTP_TLS_WIN' }


class FTP_TLS_WIN(ftplib.FTP_TLS):
    def storbinary(self, cmd, fp, blocksize=8192, callback=None, rest=None):
        """Store a file in binary mode.  A new port is created for you.

        Args:
          cmd: A STOR command.
          fp: A file-like object with a read(num_bytes) method.
          blocksize: The maximum data size to read from fp and send over
                     the connection at once.  [default: 8192]
          callback: An optional single parameter callable that is called on
                    each block of data after it is sent.  [default: None]
          rest: Passed to transfercmd().  [default: None]

        Returns:
          The response code.
        """
        self.voidcmd('TYPE I')
        with self.transfercmd(cmd, rest) as conn:
            while 1:
                buf = fp.read(blocksize)
                if not buf:
                    break
                conn.sendall(buf)
                if callback:
                    callback(buf)
            # shutdown ssl layer
            if _SSLSocket is not None and isinstance(conn, _SSLSocket):
                # http://www.sami-lehtinen.net/blog/python-32-ms-ftps-ssl-tls-lockup-fix
                # https://stackoverflow.com/questions/30629407/ftp-upload-error-with-python-and-ftplib
                # conn.unwrap()
                pass
        return self.voidresp()


class IMPL_FTP_TLS_WIN(FTP_TLS_WIN, IMPL_FTP_TLS):
    pass
