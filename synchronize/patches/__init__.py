# -*- coding: utf-8 -*-
__all__={'IMPL_FTP_TLS', 'FTP_TLS_WIN', 'IMPL_FTP_TLS_WIN'}

from synchronize.patches.implicit_ftps import IMPL_FTP_TLS
from synchronize.patches.win_tls_ftp import FTP_TLS_WIN, IMPL_FTP_TLS_WIN
