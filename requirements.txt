boto3==1.4.4
PyYAML
pylru==1.0.9
watchtower==0.3.6
python-json-logger==0.1.7
lxml
requests
paramiko==2.2.1
selenium==3.6.0
python-magic==0.4.13
# for Chrome local
splinter==0.7.7
