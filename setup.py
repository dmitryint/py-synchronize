#!/usr/bin/env python
from __future__ import print_function
from setuptools import setup, find_packages
from synchronize import __version__, __pkg_name__
import os

requires = open('requirements.txt').read().splitlines()

setup_requires = install_requires = requires

tests_require = {
    "mock",
}

"""
try:
    from cx_Freeze import setup, Executable
    executables = [
        Executable(script="synchronize/cli/yam_syncer.py",
                   base=None,
                   targetName= "py-synchronize.exe",
                   # icon= "doc/logo.ico",
                   shortcutName= "py-synchronize",
                   )
         ]
except ImportError:
    # tox has problems to install cx_Freeze to it's venvs, but it is not needed
    # for the tests anyway
    print("Could not import cx_Freeze; 'build' and 'bdist' commands will not be available.")
    print("See https://pypi.python.org/pypi/cx_Freeze")
    executables = []
"""

# 'setup.py upload' fails on Vista, because .pypirc is searched on 'HOME' path
if not "HOME" in os.environ and  "HOMEPATH" in os.environ:
    os.environ.setdefault("HOME", os.environ.get("HOMEPATH", ""))
    print("Initializing HOME environment variable to '{}'".format(os.environ["HOME"]))

build_exe_options = {
    "init_script": "Console",
    "includes": install_requires,
    }

bdist_msi_options = {
    "upgrade_code": "{62442ED5-885C-4C0F-8160-221444C71408}",
    "add_to_path": True,
    }

setup(
    name=__pkg_name__,
    version=__version__,
    # Development Status :: 2 - Pre-Alpha
    # Development Status :: 3 - Alpha
    # Development Status :: 4 - Beta
    # Development Status :: 5 - Production/Stable

    classifiers=["Development Status :: 2 - Pre-Alpha",
                 "Environment :: Console",
                 "Intended Audience :: Information Technology",
                 "Intended Audience :: Developers",
                 "License :: OSI Approved :: MIT License",
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3.3",
                 "Programming Language :: Python :: 3.4",
                 "Programming Language :: Python :: 3.5",
                 "Programming Language :: Python :: 3.6",
                 "Topic :: Software Development :: Libraries :: Python Modules",
                 "Topic :: Utilities",
                 ],
    install_requires=install_requires,
    setup_requires=setup_requires,
    tests_require=tests_require,
    packages=find_packages(exclude=['ez_setup', 'tests', 'tests.*', '.git']),
    package_data={'': [
        'chromedriver/Linux/chromedriver',
    ]},
    include_package_data=True,
    test_suite="synchronize.test",
    entry_points={
        "console_scripts": [
            "py-synchronize = synchronize.cli:yamlsync",
            "py-synchronize-queue = synchronize.cli:queue_handler",
        ],
    },
    #executables=executables,
    options={"build_exe": build_exe_options,
             "bdist_msi": bdist_msi_options,
             }
)
